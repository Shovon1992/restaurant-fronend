import React, { useRef, useState } from "react";
import {
  View,
  Text,
  TextInput,
  ActivityIndicator,
  KeyboardAvoidingView,
  Image,
  TouchableOpacity,
} from "react-native";
import styles from "../LoginScreen/loginStyle";
import { Ionicons } from "@expo/vector-icons";
import { useDispatch } from "react-redux";

import * as authActions from "../../../store/actions/authAction";
import { validateAll } from "indicative/validator";

import Theam from "../../../constants/Theme";
import CustomButton from "../../../CustomComponents/Button/Button";
import CodeInput from "react-native-confirmation-code-input";

import { useSelector } from "react-redux";
import Theme from "../../../constants/Theme";

const ConfirmSignupScreen = (props) => {
  const userName = useSelector((state) => state.auth.email);
  console.log(userName);
  const dispatch = useDispatch();

  const inputRef = useRef("codeInputRef2");

  // const [userName, setUserName] = useState('');
  const [confirmCode, setConfirmCode] = useState("");

  const [error, setError] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  const state = {
    //  userName:userName,
    confirmCode: confirmCode,
  };

  const confirmSignup = async () => {
    // const userName = props.navigation.state.params.userName;
    const rules = {
      //   userName:'required|email',
      confirmCode: "required",
    };

    const messages = {
      required: (field) => `${field} is required`,
      // 'userName.email': Message.userName,
    };

    try {
      setIsLoading(true);
      setError({});
      console.log("userName", userName, " and confirmCode ", confirmCode);
      const validateForm = await validateAll(state, rules, messages);
      if (validateForm) {
        await dispatch(authActions.confirm(userName, confirmCode));
        props.navigation.navigate("Login");
      }
    } catch (err) {
      const formattedErrors = {};
      const checkAPIResponse = Array.isArray(err);
      if (checkAPIResponse !== true) {
        formattedErrors["apiError"] = "" + err + "";
      } else {
        err.forEach((error) => (formattedErrors[error.field] = error.message));
      }

      setError(formattedErrors);
      setIsLoading(false);
    }
  };

  return (
    <View style={styles.container}>
      <KeyboardAvoidingView behavior="height">
        <View style={styles.part1}>
          <Image
            source={require("../../../assets/images/menume-logo.png")}
            style={styles.logo}
          ></Image>
        </View>
        <View style={styles.part2}>
          <View style={{ marginTop: 20 }}>
            <Text style={{ color: Theme.grayColor }}>
              Enter OTP sent on {userName}
            </Text>
          </View>
          <View style={styles.otpBoxesContainer}>
            <CodeInput
              ref={inputRef}
              // secureTextEntry
              className={"border-box"}
              activeColor={Theme.grayColor}
              inactiveColor="#bab5b1"
              space={Theme.deviceWidth / 21}
              keyboardType="numeric"
              autoFocus={true}
              codeLength={6}
              size={Theme.deviceWidth * 0.11}
              inputPosition="center"
              onFulfill={(code) => setConfirmCode(code)}
            />
          </View>
          <View>
            {/* <Ionicons
              name="md-lock"
              size={Theam.deviceHeight * 0.05}
              style={styles.InputRightIcon}
            />
            <TextInput
              style={styles.textInput}
              placeholder="OTP"
              secureTextEntry={true}
              onChangeText={(text) => {
                setConfirmCode(text);
              }}
            /> */}

            {error.password && (
              <View style={styles.errorContainer}>
                <Text style={styles.errorText}>{error.password}</Text>
              </View>
            )}
          </View>

          {isLoading ? (
            <View style={styles.loading}>
              <ActivityIndicator size="large" color="black" />
              <Text style={{ textAlign: "center" }}>Validating</Text>
            </View>
          ) : (
            <View style={styles.buttonWrapper}>
              <View style={(styles.bottomLink, { alignSelf: "flex-end" })}>
                <TouchableOpacity onPress={() => {}}>
                  <Text style={styles.bottomLinkText}>Re-Send OTP</Text>
                </TouchableOpacity>
              </View>
              {error.apiError && (
                <View style={styles.errorContainer}>
                  <Text style={styles.errorText}>{error.apiError}</Text>
                </View>
              )}
              {error.apiError && (
                <View style={styles.errorContainer}>
                  <Text style={styles.errorText}>{error.apiError}</Text>
                </View>
              )}

              <View style={(styles.buttonMainContainer, styles.loginBtn)}>
                <CustomButton
                  onPress={confirmSignup}
                  title="Cofirm OTP"
                  style={styles.loginBtnStyle}
                />
              </View>
            </View>
          )}
        </View>
      </KeyboardAvoidingView>
    </View>
  );
};

ConfirmSignupScreen.navigationOptions = {
  headerTransparent: true,
  headerTitle: "",
};

export default ConfirmSignupScreen;
