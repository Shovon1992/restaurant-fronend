import {StyleSheet} from 'react-native';
import Theme from '../../constants/Theme';
const styles = StyleSheet.create({
    screen : {
        flex : 1,
       // justifyContent : 'center',
        alignItems : 'center'
    },
    section1 : {
        justifyContent : 'center',
        alignItems : 'center',
        backgroundColor : Theme.background,
        width : '100%',
        height : '40%',
        borderBottomColor: 'grey',
        borderBottomWidth: 1,
    },
    containerTopTextWrapper : {
        alignItems : 'center',
        paddingTop : 20,
    },
    boldHeadText : {
        fontSize : 20,
        fontWeight : 'bold'
    },
    textLeft : {
        fontSize : 16,
        fontWeight : 'bold',
        justifyContent: 'center'
    },
    textRight : {
        flex: 1,
        fontSize : 14,
        flexWrap: 'wrap'
    },
    section2 : {
        padding : 20,
        width : '100%',
        borderBottomColor: 'grey',
        borderBottomWidth: 1,
    },
    section2Another : {
         borderTopWidth: 1,
         marginTop : 100,
         padding : 20,
         width : '100%',
         borderBottomColor: 'grey',
         borderBottomWidth: 1,
     },
    sectionWrapper : {
        
        flexDirection : 'row',
        justifyContent : 'space-between'
    },
    section3 : {
        padding : 20,
        width : '100%',
        borderBottomColor: 'grey',
        borderBottomWidth: 1,
    },
    section4 : {
        padding : 20,
        width : '100%',
        borderBottomColor: 'grey',
        borderBottomWidth: 1,
    },
    sectionWrapper4 : {
        
        flexDirection : 'column',
        justifyContent : 'space-between'
    },
    textRight4 :{
        paddingTop : 10
    },
    section5 : {
        padding : 20,
        width : '100%',
        borderBottomColor: 'grey',
        borderBottomWidth: 1,
    },
    section6 : {
        padding : 20,
        width : '100%',
    },
    buttonMainContainer : {
         paddingBottom : 12
    },
    backToMenuText : {
        color : Theme.greenColor,
    },
    backToMenuButton : {
        backgroundColor : '#f0f0f5',
        borderColor : Theme.greenColor,
        borderWidth : 2,
        width : Theme.deviceWidth*.9,
        height : Theme.deviceHeight*.06,
        borderRadius: 5,
        paddingVertical: Theme.deviceHeight*.005,
        paddingHorizontal: Theme.deviceWidth*.01,
    },
    button:{
        width : Theme.deviceWidth*.9,
        height : Theme.deviceHeight*.06,
        borderRadius: 5,
        paddingVertical: Theme.deviceHeight*.005,
        paddingHorizontal: Theme.deviceWidth*.01,
    },
    image : {
        height: Theme.deviceHeight*.10,
        width: Theme.deviceWidth*.20,
    },
    textTitle : {
        fontWeight : 'bold'
    }
});

export default styles;


