import React from "react";
import { StyleSheet } from "react-native";
import Theme from "../../constants/Theme";

const styles = StyleSheet.create({
  modal: {
    width: Theme.deviceWidth,
    padding: 0,
    height: Theme.deviceHeight,
    margin: 0,
  },
  modalContent: {
    padding: 0,
    margin: 0,
    height: 250,
    width: Theme.deviceWidth,
    marginTop: "auto",
    backgroundColor: "#fff",
    // padding: 10,
  },
});

export default styles;
