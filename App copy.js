import React, {useState,useEffect} from 'react';
// import AsyncStorage from 'react-native';
import Navigation from './navigation/Navigation';
import {enableScreens} from 'react-native-screens';
import Amplify, { Auth } from 'aws-amplify';
import awsconfig from './aws-exports';
// import axios from 'axios';
import * as SplashScreen from 'expo-splash-screen';

Amplify.configure(awsconfig);

//use for reducers
import {createStore, combineReducers, applyMiddleware} from 'redux';
import ReduxThunk from 'redux-thunk';
import {Provider} from 'react-redux';
/* import meals from the reducer */
import ReducerMeals from './store/reducers/mealsCategoryReducer';
import ReucerFoodItems from './store/reducers/foodItemReducer';
import ReducerWelcome from './store/reducers/welcome';
import AuthReducer from './store/reducers/authReducer';
import ReducerCart from './store/reducers/cartReducer';
import QrReducer from './store/reducers/QRcodeReducer';
import OrderReducer from './store/reducers/orderReducer';

//import PersistantLogin from './screens/Auth/PersistantLogin';

enableScreens();
 
/* using Redux to call reducers from store directory */
const rootReducer = combineReducers({
  auth : AuthReducer,
  qrcode : QrReducer,
  welcome : ReducerWelcome,
  meals : ReducerMeals,
  foodItems : ReucerFoodItems,
  cart : ReducerCart,
  orders : OrderReducer
});
 
const store = createStore(rootReducer,applyMiddleware(ReduxThunk));

export default function App() {
  const [appIsReady, setAppisReady] = useState(false);
  const [authenticating, setAuthenticating] = useState(false);


  useEffect(()=>{
    fetchData();
  },[fetchData]);

  const fetchData = async () => {
    try {
      await SplashScreen.preventAutoHideAsync();
    }
    catch (err){
      //console.log(err);
    }
    prepareResources();
  }
  const prepareResources = async () => {
    await performAPICalls();
  }

  const performAPICalls = async ()=> {
    // try{
    //  //
    // }catch(error){
    //   setAuthStatus(false);
    //   setUser();
    //   console.log('auth status',error);
    // }
   
    setAppisReady(true);
    await SplashScreen.hideAsync();
  }

  if(!appIsReady){
    return null;
  }
  
    return (
      <Provider store={store}>
          <Navigation/>
      </Provider>
      
    ); 
  
 
}


