import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Modal,
  Image,
  Easing,
  Animated,
  Text,
  ImageBackground,
} from "react-native";

const Loader = (props) => {
  const { loading } = props;
  let spinAnim = new Animated.Value(0);
  Animated.loop(
    Animated.timing(spinAnim, {
      toValue: 1,
      duration: 3000,
      easing: Easing.spin,
      useNativeDriver: true,
    })
  ).start();

  let spin = spinAnim.interpolate({
    inputRange: [0, 1],
    outputRange: ["0deg", "360deg"],
  });

  return (
    <Modal
      transparent={true}
      animationType={"none"}
      visible={loading}
      onRequestClose={() => {
        // console.log("close modal");
      }}
    >
      <View style={styles.modalBackground}>
        <View style={styles.activityIndicatorWrapper}>
          <ImageBackground
            style={{ height: 200, width: 200 }}
            source={require("../../assets/images/loader-icon.png")}
          >
            <View
              style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Animated.Image
                style={{
                  height: 150,
                  width: 150,
                  transform: [{ rotate: spin }],
                }}
                source={require("../../assets/images/menume-logo.png")}
              />
              {/* <Image
            style={{height:200, width: 200}}
            source={require('../../assets/icon.png')} /> */}
              {/* <Text
              style={{
                color: '#fff',
                fontSize: 20,
                fontWeight: 'bold',
                top: 50,
                left: 55
              }}
            >
              Loading
            </Text> */}
            </View>
          </ImageBackground>
          {/* <ActivityIndicator
            animating={loading} /> */}
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  modalBackground: {
    flex: 1,
    alignItems: "center",
    flexDirection: "column",
    justifyContent: "space-around",
    backgroundColor: "rgba(35, 30, 11, 0.8)",
  },
  activityIndicatorWrapper: {
    //backgroundColor: '#FFFFFF',
    height: 100,
    width: 100,
    borderRadius: 10,
    display: "flex",
    alignItems: "center",
    justifyContent: "space-around",
  },
});

export default Loader;
