export default class Fooditems {
    constructor(id,thumbUrl,description,name,taste,veg,availabilityStatus,quantity,priceId,price,currency,unit){
        this.id  = id,
        this.thumbUrl = thumbUrl,
        this.description = description,
        this.name = name,
        this.taste = taste,
        this.veg = veg,
        this.availabilityStatus = availabilityStatus,
        this.quantity = quantity,
        this.priceId = priceId,
        this.price = price,
        this.currency = currency,
        this.unit = unit
    }
}