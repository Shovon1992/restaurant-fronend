import { StyleSheet, Dimensions } from "react-native";
import Theme from "../../../constants/Theme";
var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;

const styles = StyleSheet.create({
  gridItems: {
    flex: 1,
    flexDirection: "row",
    margin: 5,
    height: deviceHeight * 0.15,
    borderRadius: 1,
    elevation: 2,
    shadowColor: "black",
    shadowOpacity: 0.12,
    shadowOffset: { width: 0, height: 1 },
    width: deviceWidth * 0.98,
    alignSelf: "center",
  },
  itemImage: {
    resizeMode: "cover",
    height: Theme.deviceHeight * 0.15 * 0.96,
    width: Theme.deviceHeight * 0.15 * 0.96,
    alignSelf: "center",
    top: 1,
    // left : 5
  },
  bgImage: {
    resizeMode: "contain",
    height: deviceHeight * 0.25,
    width: deviceWidth,
  },
  titleContainer: {
    backgroundColor: "rgba(0,0,0,0.5)",
    paddingVertical: 5,
    paddingHorizontal: 12,
    position: "absolute",
    bottom: 0,
    left: 0,
    right: 0,
  },
  title: {
    fontSize: Theme.headerTextSize,
    textAlign: "left",
    color: "white",
  },
  cartBtn: {
    position: "absolute",
    width: Theme.deviceWidth * 0.5,
    height: Theme.deviceHeight * 0.5,
    alignItems: "center",
    justifyContent: "center",
    right: 30,
    bottom: 30,
    backgroundColor: "green",
    zIndex: 99999,
    padding: 10,
    borderRadius: 60,
  },
  itemInfo: {
    flex: 2,
    flexDirection: "column",
    top: 4,
    left: 10,
    height: Theme.deviceHeight * 0.15 * 0.5,
  },
  itemHeader: {
    fontSize: Theme.headerTextSize,
    fontWeight: "bold",
  },
  itemDetails: {
    fontSize: Theme.subheader,
    color: "gray",
  },
  itemPrice: {
    fontSize: Theme.textSize,
    fontWeight: "bold",
    color: "#71c90c",
    top: Theme.deviceHeight * 0.15 * 0.1,
  },
  itemAction: {
    alignContent: "center",
    flex: 1,
    flexDirection: "column",
    top: Theme.deviceHeight * 0.15 * 0.37,
    //alignContent: 'space-between'
    left: Theme.deviceWidth * 0.05,
    // backgroundColor: 'red'
  },
  addBtn: {
    borderColor: "#71c90c",
    borderRadius: 5,
    borderWidth: 2,
    width: Theme.deviceWidth * 0.15,
  },
});

export default styles;
