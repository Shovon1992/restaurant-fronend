import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
    card : {
        height : 320,
        width : 320,
        borderRadius : 45,
        shadowColor : 'black',
        shadowOpacity : 0.26,
        shadowOffset : {width : 0, height : 2},
        backgroundColor : 'white',
        padding : 15,
    }
});

export default styles;