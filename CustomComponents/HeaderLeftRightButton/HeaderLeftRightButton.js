import React from "react";
import { HeaderButtons, Item } from "react-navigation-header-buttons";
import customHeaderButton from "../../CustomComponents/HeaderButton/HeaderButton";

import CartCounterHeader from "../../CustomComponents/CartCounterHeader/cartCounterHeader";

const HeaderLeftRightButton = (props) => {
  /* count the number of items on cart */
  const routeName = props.navigation.state.routeName;
  // console.log(routeName);
  let cartCounter = props.navigation.getParam("countCartItems");
  if (typeof cartCounter === "undefined") {
    cartCounter = 0;
  }

  var isModalVisible = false;

  const HeaderRight = () => (
    <HeaderButtons HeaderButtonComponent={customHeaderButton}>
      <Item
        title="Notification"
        iconName="ios-notifications"
        onPress={() => {
          props.navigation.navigate("Notification");
        }}
      />
      <Item
        title="Cart"
        iconName="ios-cart"
        onPress={() => {
          props.navigation.navigate("Cart");
        }}
      />

      {cartCounter != 0 && <CartCounterHeader counter={cartCounter} />}
    </HeaderButtons>
  );
  const HeaderLeft = () => (
    <HeaderButtons HeaderButtonComponent={customHeaderButton}>
      <Item
        title="Menu"
        iconName="ios-menu"
        onPress={() => {
          props.navigation.toggleDrawer();
        }}
      />
    </HeaderButtons>
  );

  if (
    routeName == "Category" ||
    routeName == "Thankyou" ||
    routeName == "Cart"
  ) {
    return {
      headerLeft: HeaderLeft,
      headerRight: HeaderRight,
    };
  } else {
    return {
      headerRight: HeaderRight,
    };
  }
};

export default HeaderLeftRightButton;
