import React, { useEffect,useCallback } from 'react';
import * as cartActions from '../../store/actions/cartAction';
import { useSelector,useDispatch } from 'react-redux';
const fetchCartForHeader = props => {
   // console.log('fetchCartForHeader',props)
    const dispatch = useDispatch();
 // /* On landing load check for Cart Items  */
 const getCartItems = useSelector(state => state.cart.items);
 const countCartItems =  Object.keys(getCartItems).length;
 const loadProducts = useCallback( async () => {
     try{
        await dispatch(cartActions.fetchCartItems());
     }catch (err) {
        console.log(err);
     }
 },[dispatch])

   /* Resist Rerender for loading products */
 useEffect(()=>{
     loadProducts()
     props.navigation.setParams({countCartItems : countCartItems});
 }, [dispatch,loadProducts,countCartItems]);

}

export default fetchCartForHeader;

