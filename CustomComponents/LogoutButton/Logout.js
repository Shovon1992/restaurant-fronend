import React from 'react';
import { SafeAreaView, View, TouchableOpacity,Text } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { DrawerItems } from 'react-navigation-drawer';
import * as AuthAction from '../../store/actions/authAction';

import { useDispatch } from 'react-redux';

const LogoutDrawerButton = props => {
    const dispatch = useDispatch();
    return (
        <View style={{ flex: 1, paddingTop : 30 }}>
            <SafeAreaView forceInset={{ top: 'always', horizontal: 'never' }}  >
             <DrawerItems {...props.property} />
                        <TouchableOpacity
                             onPress={() => { 
                                dispatch(AuthAction.logout()) 
                                props.property.navigation.navigate('Auth')
                            }}
                        >
                                <View style={{flexDirection:'row', padding: 15}}>
                                   <Ionicons
                                        name='md-log-out'
                                        size={25}
                                        color='grey'
                                        style={{paddingHorizontal : 5}}
                                    />
                                    <Text style={{fontWeight : 'bold',paddingHorizontal : 30}}>Logout </Text>
                        </View>
                      </TouchableOpacity>
            </SafeAreaView>
        </View>
    )
}

export default LogoutDrawerButton;