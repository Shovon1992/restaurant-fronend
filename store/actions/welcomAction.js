import config from "../../constants/config";

export const WELCOMESCREEN = "WELCOMESCREEN";

export const welcomescreen = (restaurantId, tableNo) => {
  console.log("restaurantId---222", restaurantId);
  return async (dispatch, getState) => {
    //console.log('khsdkfghk kutta')
    try {
      let response = await fetch(
        config.restaurantWelcome + "/" + restaurantId,
        {
          method: "GET",
          headers: config.headers,
        }
      );
      response = await response.json();

      let userId = getState().auth.userId;
      let tableId = getState().info.selectedTable.tableNumber;
      let bookingStatus = 1;
      let response2 = await fetch(config.bookTable, {
        method: "POST",
        headers: config.headers,
        body: JSON.stringify({
          tableId,
          userId,
          bookingStatus,
        }),
      });
      response2 = await response2.json();
      console.log(
        "response welcome------",
        response2,
        getState().info.selectedTable
      );
      if (response.statusCode == 200) {
        dispatch({
          type: WELCOMESCREEN,
          data: response.data,
          restrauntInfo: {
            id: restaurantId,
            tableNo: tableNo,
          },
        });
      } else {
        throw error;
      }
    } catch (error) {
      console.log("action error ", error);
    }
  };
};
