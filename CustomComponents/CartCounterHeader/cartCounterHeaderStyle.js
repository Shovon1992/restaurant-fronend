import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
    container : {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        bottom : 6,
        right : 10,
    },
     outerCircle: {
        borderRadius: 30,
        width: 22,
        height: 22,
        backgroundColor: 'black',
     },
    innerCircle: {
        borderRadius: 25,
        margin: 1.5,
        backgroundColor: '#FCFDDA'
    },
    text : {
        color : 'black',
        textAlign: 'center',
        fontWeight : 'bold'
    }
});

export default styles;