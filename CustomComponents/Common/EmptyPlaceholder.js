import React, { useState, useEffect } from "react";
import { View, Text, TouchableOpacity, StatusBar } from "react-native";
import Theme from "../../constants/Theme";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import styles from "./commonStyle";
import CustomButton from "../Button/Button";
import Message from "../../constants/Message";
const EmptyPlaceholder = (props) => {
  const [buttonMessage, setButtonMessage] = useState(<View></View>);
  const [errorMessage, setErrorMessage] = useState(Message.defaultEmptyMessage);
  useEffect(() => {
    console.log(props);
    switch (props.page) {
      case "cart":
        setErrorMessage(Message.emptyCart);
        setButtonMessage(
          <View style={styles.buttonMainContainer}>
            <CustomButton
              onPress={() => {
                props.navigation.navigate("Category");
              }}
              title={Message.empltyCartBtn}
              style={styles.button}
            />
          </View>
        );
    }
  }, []);
  return (
    <View style={{ flex: 1, alignContent: "center", justifyContent: "center" }}>
      <StatusBar backgroundColor={Theme.background} barStyle="dark-content" />
      <View style={{ justifyContent: "center", marginTop: -100 }}>
        <Icon
          name="emoticon-sad-outline"
          size={100}
          style={{ alignSelf: "center", color: "gray" }}
        ></Icon>
        <Text style={{ textAlign: "center", color: "gray", fontSize: 25 }}>
          {errorMessage}
        </Text>
        {buttonMessage}
      </View>
    </View>
  );
};

export default EmptyPlaceholder;
