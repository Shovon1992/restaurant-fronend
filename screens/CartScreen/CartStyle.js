import { StyleSheet } from "react-native";
import Theme from "../../constants/Theme";

const styles = StyleSheet.create({
  gridItems: {
    flex: 1,
    flexDirection: "row",
    height: Theme.deviceHeight * 0.15,
    // borderRadius : 1,
    // elevation : 2,
    // shadowColor : 'black',
    // shadowOpacity : 0.12,
    // shadowOffset : {width : 0, height : 1},
    borderBottomWidth: 1,
    borderBottomColor: "gray",
    width: Theme.deviceWidth,
    //padding: 5,
    //margin: 5
  },
  itemImage: {
    resizeMode: "cover",
    height: Theme.deviceHeight * 0.1,
    width: Theme.deviceWidth * 0.2,
    alignSelf: "center",
    top: 1,
    left: 5,
    padding: 10,
  },
  bgImage: {
    resizeMode: "contain",
    height: Theme.deviceHeight * 0.25,
    width: Theme.deviceWidth,
  },
  // titleContainer:{
  //     backgroundColor : 'rgba(0,0,0,0.5)',
  //     paddingVertical : 5,
  //     paddingHorizontal : 12,
  //     position : 'absolute',
  //     bottom: 0,
  //     left: 0,
  //     right: 0
  // },
  // title : {
  //     fontSize : 20,
  //     textAlign  : 'center',
  //     color : 'white'
  // },
  // cartBtn :{
  //     position: 'absolute',
  //     width: 60,
  //     height: 60,
  //     alignItems: 'center',
  //     justifyContent: 'center',
  //     right: 30,
  //     bottom: 30,
  //     backgroundColor: 'green',
  //     zIndex: 99999,
  //     padding: 10,
  //     borderRadius: 60
  // },
  itemInfo: {
    flex: 2,
    flexDirection: "column",
    top: Theme.deviceHeight * 0.02,
    left: Theme.deviceWidth * 0.05,
    height: Theme.deviceHeight * 0.25,
  },
  itemHeader: {
    fontSize: Theme.headerTextSize,
    fontWeight: "bold",
    marginBottom: Theme.deviceHeight * 0.02,
  },
  // itemDetails :{
  //     fontSize: 15,
  //     color: 'gray'
  // },
  itemPrice: {
    fontSize: Theme.headerTextSize,
    fontWeight: "bold",
    color: Theme.greenColor,
    position: "absolute",
  },
  itemAction: {
    flex: 1,
    flexDirection: "row",
    alignContent: "center",
    top: Theme.deviceHeight * 0.05,
    left: Theme.deviceWidth * 0.005,
  },
  summarySection: {
    flex: 1,
    height: 430,
  },
  sugesstionContainer: {
    padding: 20,
  },
  TextInputStyleClass: {
    // textAlign: 'center',
    height: 50,
    borderWidth: 2,
    borderColor: "#9E9E9E",
    borderRadius: 5,
    backgroundColor: "#FFFFFF",
    height: 100,
  },
  itemSummary: {
    justifyContent: "center",
    borderTopWidth: 1,
    borderTopColor: "gray",
    borderBottomWidth: 1,
    borderBottomColor: "gray",
    padding: 20,
  },
  commonItems: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingTop: 10,
  },
  commonText: {
    fontWeight: "bold",
  },
  TotalConatainer: {
    //  paddingBottom : 0,
    flexDirection: "row",
    justifyContent: "space-between",
    padding: 20,
  },
  totalPrice: {
    color: Theme.greenColor,
  },
  proceedBtn: {
    // height: Theme.deviceHeight*.08,
    width: Theme.deviceWidth,
    backgroundColor: Theme.greenColor,
    padding: Theme.deviceHeight * 0.02,
    textAlign: "center",
    position: "absolute",
    left: 0,
    right: 0,
    bottom: 0,
    zIndex: 999999,
  },
  proceedText: {
    textAlign: "center",
    fontSize: Theme.headerTextSize,
    color: "#fff",
  },
  quantityView: {
    marginTop: 5,
    justifyContent: "center",
    borderColor: Theme.greenColor,
    borderWidth: 1,
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    borderRadius: 2,
    backgroundColor: Theme.greenColor,
  },
  quantityButton: {
    color: "#fff",
    // bottom: 3,
  },
});

export default styles;
