export default class Welcome {
    constructor(image,header,description){
        this.header = header,
        this.image = image,
        this.description = description
    }
}