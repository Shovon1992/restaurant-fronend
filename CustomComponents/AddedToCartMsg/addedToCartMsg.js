import React from 'react';
import { View, Text} from 'react-native';
import styles from './addedToCartStyle';
const addedToCartMsg = props => {
    return (
        <View style={styles.container}>
                     <Text style={styles.message}>{props.msg}</Text>  
        </View>
    )
}

export default addedToCartMsg;