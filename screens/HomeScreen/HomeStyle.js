import { StyleSheet } from "react-native";
import Theme from "../../constants/Theme";
const styles = StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: Theme.background,
  },
  headerLogo: {
    flexDirection: "row",
    alignSelf: "center",
  },

  logo: {
    borderRadius: 100,
    height: 150,
    width: 150,
    alignSelf: "flex-start",
  },
  textContainer: {
    justifyContent: "center",
    alignContent: "center",
    padding: 30,
  },
  text: { justifyContent: "center", textAlign: "center" },
  buttonWrapper: {
    paddingTop: 20,
  },
  buttonMainContainer: {
    paddingBottom: 20,
  },
  button: {
    width: Theme.deviceWidth * 0.75,
    height: Theme.deviceHeight * 0.06,
    borderRadius: 5,
    paddingVertical: 2, //Theme.deviceHeight*.002,
    paddingHorizontal: 2, //Theme.deviceWidth*.01,
  },
  registerButton: {
    backgroundColor: Theme.registerButton,
    borderColor: Theme.greenColor,
    borderWidth: 2,
    height: Theme.deviceHeight * 0.06,
    borderRadius: 5,
    // paddingVertical: Theme.deviceHeight*.003,
    // paddingHorizontal: Theme.deviceWidth*.02,
  },
  registerButtonText: {
    color: Theme.greenColor,
  },
});

export default styles;
