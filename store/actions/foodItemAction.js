import Fooditems from '../../model/fooditems';
import config from "../../constants/config";

export const FETCH_FOOD_ITEMS = 'FETCH_FOOD_ITEMS';

export const fetchFoodItems = (catagoryId) =>{

    return async dispatch => {
        let response = await fetch(config.foodItems+'/'+catagoryId,{
            method: 'GET',
            headers: config.headers
        });
        let resData = await response.json();

        console.log('foodItems data are',resData);
        let tempItem = [];
       
        resData.data.map((element, key) =>{
            
        tempItem.push(
                new Fooditems(
                    element.hash_id,
                    element.thumbUrl,
                    element.description,
                    element.name,
                    element.taste,
                    element.veg,
                    element.availabilityStatus,
                    element.quantity,
                    element.priceId,
                    element.price,
                    element.currency,
                    element.unit
                )
            );
        });
        
        dispatch({ type: FETCH_FOOD_ITEMS, fooditems : tempItem })

    }

}