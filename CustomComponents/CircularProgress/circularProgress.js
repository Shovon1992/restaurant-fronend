import React, { useEffect, useState } from "react";
import { View, StyleSheet, Text } from "react-native";
import { AnimatedCircularProgress } from "react-native-circular-progress";
import Theme from "../../constants/Theme";

const CircularProgress = (props) => {
  const [fill, setFill] = useState(0);
  const [timeDiff, setTimeDiff] = useState(0);
  const [totalWaitingTime, setTotalWaitingTime] = useState(0);

  const timer = () => {
    const orderTime = new Date();
    orderTime.setHours(
      orderTime.getHours(),
      orderTime.getMinutes() + 10,
      orderTime.getSeconds(),
      0
    );

    const currentTime = new Date();
    /*console.log("order Time: ", orderTime);
    console.log("current Time : ", new Date());*/

    const timeDiff = Math.ceil(((orderTime - currentTime) / (1000 * 60)) * 60);

    setTotalWaitingTime(
      Math.ceil(((orderTime - currentTime) / (1000 * 60)) * 60)
    );

    setTimeDiff(timeDiff);

    let interval = setInterval(() => {
      setTimeDiff((lastTimerCount) => {
        lastTimerCount <= 1 && clearInterval(interval);
        return lastTimerCount - 1;
      });
      setFill(timeDiff);
    }, 1000); //each count lasts for a second
    //cleanup the interval on complete
    return () => clearInterval(interval);
  };

  /*console.log("time diff : ", timeDiff);
  console.log("total waiting time diff : ", totalWaitingTime);

  console.log("fill % : ", fill);*/

  useEffect(() => {
    timer();
  }, []);

  return (
    <View style={styles.container}>
      <AnimatedCircularProgress
        size={70}
        width={5}
        fill={parseInt((totalWaitingTime - timeDiff) * 100) / totalWaitingTime}
        tintColor={"#d9dbde"}
        duration={2500}
        rotation={360}
        // backgroundColor="#d9dbde"
        backgroundColor={Theme.greenColor}
      >
        {(fill) => (
          <View>
            <Text>{Math.ceil(timeDiff / 60)}</Text>
          </View>
        )}
      </AnimatedCircularProgress>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: 70,
    height: 70,
    // borderWidth: 5,
    // borderRadius: 100,
    // borderColor: "#eba205",
    justifyContent: "center",
    alignItems: "center",
  },
});

export default CircularProgress;
