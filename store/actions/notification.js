import config from "../../constants/config";

export const NOTIFICATION_ADD = "NOTIFICATION_ADD";
export const NOTIFICATION_GET = "NOTIFICATION_GET";

export const notificationAdd = (restaurantId, userId, message, table) => {
  return async (dispatch) => {
    try {
      const res = await fetch(config.callWaiter, {
        method: "POST",
        headers: config.headers,
        body: JSON.stringify({
          receiver: restaurantId,
          receiver_type: "restaurant",
          sender: userId,
          sender_type: "user",
          message: message,
          table: table,
        }),
      }).then(
        (res1) => {
          console.log(res1);
        },
        (error) => {
          console.log(error);
        }
      );
      res = res.json();
      console.log(
        "clll waiter =======",
        {
          receiver: restaurantId,
          receiver_type: "restaurant",
          sender: userId,
          sender_type: "user",
          message: message,
          table: table,
        },
        res
      );
    } catch (error) {
      console.log("action Error : ", error);
      throw new Error(error.message);
    }
  };
};

export const notificationGet = (userId) => {
  return async (dispatch) => {
    try {
      const res = await fetch(config.getNotification + "/" + userId, {
        method: "GET",
        headers: config.headers,
      });

      const resData = await res.json();
      // console.log("response Data : ", resData.data[0]);
      if (resData) {
        await dispatch({
          type: "NOTIFICATION_GET",
          data: resData.data[0],
        });
      }
    } catch (error) {
      // console.log("Action Error get :", error);
      throw new Error(error.message);
    }
  };
};
