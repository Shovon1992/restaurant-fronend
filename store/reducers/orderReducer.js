import { ADD_ORDER,FETCH_ORDER } from '../actions/orderAction';
import Order from '../../model/orders';

const inititalState = {
    orders : [],
    orderId : '',
    totalAmount : 0
}

export default (state = inititalState, action)=>{
    switch(action.type){
        case FETCH_ORDER:
            return {
                orders : action.orders,
                totalAmount : action.totalAmount
            }
        case ADD_ORDER:
            const newOrder = new Order(
                action.orderData.id, 
                action.orderData.items, 
                action.orderData.amount,
                action.orderData.date, 
            )
            return {
                ...state,
                orders : state.orders.concat(newOrder),
                orderId : action.orderId
            }
    }

    return state;
}