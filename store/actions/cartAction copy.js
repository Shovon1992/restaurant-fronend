export const FETCH_CART = 'FETCH_CART';
export const ADD_TO_CART = 'ADD_TO_CART';
export const REMOVE_QTY_FROM_CART = 'REMOVE_QTY_FROM_CART';
export const ADD_QTY_TO_CART = 'ADD_QTY_TO_CART';
export const EMPTY_CART = 'EMPTY_CART';

import config from "../../constants/config";
import Cart from '../../model/cart';

export const fetchCartItems = () => {
    console.log('fetchCartItems');
    return async (dispatch,getState) => {
        
        const userId = getState().auth.userId ? getState().auth.userId : '1a1b94d6d0f6f890e47d044aa9bf6ee7a72afaee';
        const restrauntInfo = getState().welcome.restrauntInfo;

        console.log('fetchCartItems userId',userId);
        console.log('fetchCartItems restrauntId',restrauntInfo.id);
        console.log('fetchCartItems tableNo',restrauntInfo.tableNo);
        try {
            const response = await fetch(
                config.cartFetch+'/'+userId+'/'+restrauntInfo.id+'/13',
                {
                    method: 'GET',
                    headers: config.headers,
                });
            const resData = await response.json();
            console.log('resData',resData);    
            const loadedProducts = [];
            for(const key in resData['data']){
               loadedProducts.push(new Cart(
                    resData['data'][key]['cartId'],
                    resData['data'][key]['cartItemId'],
                    resData['data'][key]['name'],
                    resData['data'][key]['qty'],
                    resData['data'][key]['price'],
                // resData['data'][key]['image'], // Image 
                    'https://cdn.loveandlemons.com/wp-content/uploads/2020/03/how-to-cook-rice.jpg',
                    resData['data'][key]['price'] // product SUM

                ));
            }
             console.log('cart fetch API loadedProducts',loadedProducts);
             dispatch({
                type : FETCH_CART,
                items : loadedProducts
            })

        } catch (error) {
           throw new Error(error);
        }

      
    }
}


export const addToCart = product => {
    return async (dispatch,getState) => { 
        
        const userId = getState().auth.userId ? getState().auth.userId : '1a1b94d6d0f6f890e47d044aa9bf6ee7a72afaee';
        const restrauntInfo = getState().welcome.restrauntInfo;

        try {
            const response = await fetch(
                    config.cart,
                    {
                        method: 'POST',
                        headers: config.headers,
                        body: JSON.stringify({
                            userId : userId, 
                            restaurantId : restrauntInfo.id, 
                          //  tableId:restrauntInfo.tableNo,
                            tableId : 13,
                            foodItemId : product.id,
                            priceId : product.priceId,
                            qty : product.quantity
                        })
                 });

            const resData = await response.json();
            
            dispatch({ type : ADD_TO_CART,  product : product })  
              
        } catch (error) {
            console.log(error);
            throw new Error(error);
        }

       
    };
};

export const removeQtyFromCart = itemId => {
    return async (dispatch,getState) => {
        const restrauntInfo = getState().welcome.restrauntInfo;

        const response = await fetch('https://hfkwxrsz67.execute-api.us-east-1.amazonaws.com/dev/api/v1/restaurant/food/cart',{
            method: 'PATCH',
            headers: config.headers,
            body: JSON.stringify({
                cartId : 'b16c848af926e728fabafd4e2086b0c22d200be5', 
                tableId : 13,
                foodItemId : 'e7135de557773f1bc58ebfe35ac84b69e2afd669',
            })  
        })
        
        const resData = await response.json();
        console.log('response',resData);


        //dispatch({ type : REMOVE_QTY_FROM_CART, pid : itemId })     
    }
}

export const addQtyToCart = (productId) => {
    return {type : ADD_QTY_TO_CART, pid : productId }
}


export const emptyCart = () => {
    return {type : EMPTY_CART}
}