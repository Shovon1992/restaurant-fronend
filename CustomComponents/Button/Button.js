import React from 'react';
import {TouchableOpacity,Text, View, StyleSheet} from 'react-native';
import styles from './ButtonStyle';

const CustomButton = props => {
    return (
        <View style={{...styles.buttonContainer,...props.style}}>
                        <TouchableOpacity onPress={props.onPress} disabled={props.disabled}>
                            <View style={{...props.IconButtonStyle}}>
                                        {props.children} 
                                <Text style={{...styles.buttonText,...props.buttonText}}>
                                        {props.title} 
                                </Text>
                            </View>
                           
                        </TouchableOpacity>
                    </View>
    )
    
}

export default CustomButton;