import React from "react";
import { View, Text, ImageBackground, Button } from "react-native";
import styles from "./CarouselStyle";

export const Slide = (props) => {
  const { details } = props;
  //   console.log('slide page ', props.restaurantId)
  return (
    <View style={styles.slide}>
      <View style={styles.card}>
        <ImageBackground
          source={{ uri: details.image }}
          style={styles.cardImage}
        />
        <View style={styles.cardDescription}>
          <Text style={styles.cardTitle}>{details.header}</Text>
          <Text style={styles.cardContent}>{details.description}</Text>
        </View>

        <View style={styles.button}>
          <Button
            title="Get Started !"
            color="#71c90c"
            onPress={() => {
              props.navigation.navigate({
                routeName: props.navigateTo,
                params: {
                  restaurantId: props.restaurantId,
                },
              });
            }}
          />
        </View>
      </View>
    </View>
  );
};

export default Slide;
