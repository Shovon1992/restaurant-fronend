import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
    screen : {
        flex : 1,
        justifyContent : 'center',
        padding : 15,
        borderBottomColor: 'grey',
        borderBottomWidth: 1,
    },
    orderId :{
        fontWeight : 'bold',
        fontSize : 16
    },
    statusText : {
        color : 'green',
        fontWeight : 'bold',
        fontSize : 12
    },
    section1 : {
        flex : 1,
        flexDirection : 'row',
        justifyContent : 'space-between'
    },
    section3 : {
        flexDirection : 'row',
        justifyContent : 'space-between'
    },
    commonSection : {
        paddingTop : 6
    },
    commonText : {
        color : 'grey',
       // fontWeight : 'bold',
        fontSize : 12
    },
    buttonStyle : {
        width : 100,
        height : 34,
        borderRadius : 5
    },
    buttonText : {
        fontSize : 16
    }
});

export default styles;