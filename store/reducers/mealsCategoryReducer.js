
import {FETCH_CATEGORY} from '../actions/mealsCategoryAction';

const initialState = {
    categories : null,
}

export default (state = initialState, action) => {
    switch(action.type){
        case FETCH_CATEGORY :
            return {
                    categories : action.categories
                }
        default : 
            return state;
    }
}
