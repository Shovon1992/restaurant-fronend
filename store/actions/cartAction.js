export const FETCH_CART = 'FETCH_CART';
export const ADD_TO_CART = 'ADD_TO_CART';
export const REMOVE_QTY_FROM_CART = 'REMOVE_QTY_FROM_CART';
export const ADD_QTY_TO_CART = 'ADD_QTY_TO_CART';
export const EMPTY_CART = 'EMPTY_CART';

import config from "../../constants/config";
import Cart from '../../model/cart';

export const fetchCartItems = () => {
    return async (dispatch,getState) => {
        const userId = getState().auth.userId;
        const restrauntInfo = getState().welcome.restrauntInfo;
        const selectedTable = getState().info.selectedTable
        try {
            const response = await fetch(
                config.cartFetch+'/'+restrauntInfo.id+'/'+selectedTable.tableNumber+'/'+userId,
                {
                    method: 'GET',
                    headers: config.headers,
                });
            const resData = await response.json();
               
                console.log('FETCH_CART',resData, config.cartFetch+'/'+restrauntInfo.id+'/'+restrauntInfo.tableNo+'/'+userId, );
            let loadedProducts = [];
            let totalAmount = 0;

            for(const key in resData['data']){
                loadedProducts.push({
                    item : resData['data'][key]['name'],
                    price  : resData['data'][key]['price'],
                    quantity : resData['data'][key]['addedQuantity'],
                    image : resData['data'][key]['foodThumbUrl'],
                    sum : resData['data'][key]['price'],
                    foodItemId : resData['data'][key]['foodItemId'],
                    priceId : resData['data'][key]['priceId'],
                    cartId : resData['data'][key]['cartId']
                });
                totalAmount += resData['data'][key]['price'] * resData['data'][key]['addedQuantity'];
            }
       
             dispatch({
                type : FETCH_CART,
                items : loadedProducts,
                totalAmount : totalAmount
            })

        } catch (error) {
           throw new Error(error);
        }

      
    }
}


export const addToCart = product => {
    return async (dispatch,getState) => { 
        const userId = getState().auth.userId;
        const restrauntInfo = getState().welcome.restrauntInfo;
        const selectedTable = getState().info.selectedTable
        console.log('dfgdfgdf34534534534543534 b    ADD_TO_CART',getState().auth,getState().welcome,getState().info);
    //    console.log('ADD_TO_CART',restrauntInfo);
        /* call ADD_TO_CART API */
            const response = await addCartAPI(
                userId,
                restrauntInfo.id,
                selectedTable.tableNumber,
                product.id,
                
                product.quantity
            );
        /* end call ADD_TO_CART API */

       console.log('ADD_TO_CART',response);

            if(!response.error){
                dispatch({ 
                        type : ADD_TO_CART,  
                        product : product
                })
            
            }
      
    };
};

export const removeQtyFromCart = (productId, product) => {
    return async (dispatch,getState) => {
        const userId = getState().auth.userId;
        const restrauntInfo = getState().welcome.restrauntInfo;
             /* call ADD_TO_CART API */
                const response = await addCartAPI(
                    userId,
                    restrauntInfo.id,
                    restrauntInfo.tableNo,
                    product.foodItemId,
                    product.priceId,
                    -1 //quantity to be removed from total   
            );
            /* end call ADD_TO_CART API */
            if(!response.error){
                dispatch({ type : REMOVE_QTY_FROM_CART, pid : productId });
            }
            else{
                console.log(response);
            }
      
    }
}

/* ADD QTY TO CART HANDLER FUNCTION */
export const addQtyToCart = (productId, product) => {
    return async (dispatch,getState) => {
        const userId = getState().auth.userId;
        const restrauntInfo = getState().welcome.restrauntInfo;
        
         /* call ADD_TO_CART API */
         const response = await addCartAPI(
                userId,
                restrauntInfo.id,
                restrauntInfo.tableNo,
                product.foodItemId,
                product.priceId,
                +1 //quantity to be added from total
            );
         /* end call ADD_TO_CART API */
            if(!response.error){
                dispatch({ type : ADD_QTY_TO_CART, pid : productId  });
            }
            else{
                console.log(response);
            }
      
    }
}

/* EMPTY CART REDUCER STATE */
export const emptyCart = () => {
    return {type : EMPTY_CART}
}


/* ADD_TO_CART API CALL FUNCTION */
const addCartAPI = async (userId, resturantId, tableId, foodItemId,qty) => { 
    
        try {
            const response = await fetch(
                    config.cart,
                    {
                        method: 'POST',
                        headers: config.headers,
                        body: JSON.stringify({
                            
                            resturantId,
                            userId, 
                            foodItemId,
                            tableId,
                            qty
                        })
                 });
                 
           const resData = await response.json();
           console.log ('resData------------------>', resData)
           return resData;
              
        } catch (error) {
            return error;
    }
   
}