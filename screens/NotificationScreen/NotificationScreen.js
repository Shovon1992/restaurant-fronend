import React, { useEffect, useState } from "react";
import { View, Text, ScrollView, Image } from "react-native";

import styles from "./NotificationStyle";
import CallWaiter from "../../CustomComponents/CallWaiter/CallWaiter";
import { useDispatch, useSelector } from "react-redux";

import * as Notification from "../../store/actions/notification";
import Loader from "../../CustomComponents/Loader/loader";

const NotificationScreen = (props) => {
  const dispatch = useDispatch();
  const [isLoading, setIsLoading] = useState(false);
  const notifications = useSelector((state) => state.notification.notification);
  const userId = useSelector((state) => state.auth.userId);

  useEffect(() => {
    setIsLoading(true);
    dispatch(Notification.notificationGet(userId));
    setIsLoading(false);
  }, []);

  const dateFormatting = (date) => {
    const Day = new Date(date).getDate();
    const Month = new Date(date).getMonth() + 1;
    const Year = new Date(date).getFullYear();
    const Hour = new Date(date).getHours();
    const Minute = new Date(date).getMinutes();
    // const Second = new Date(date).getSeconds();

    const dd = Day + "/" + Month + "/" + Year + " " + Hour + ":" + Minute;
    return dd.toString();
  };
  if (isLoading || notifications == null) {
    return <Loader></Loader>;
  } else {
    return (
      <View style={styles.container}>
        <ScrollView
          style={{ backgroundColor: "#fff" }}
          contentContainerStyle={{ paddingBottom: 170 }}
        >
          {notifications.map((data, key) => {
            if (data == null) {
              return <Loader></Loader>;
            } else if (data.sender_type === "user") {
              return (
                <View style={styles.userNtf} key={key}>
                  <View>
                    <View style={styles.userNtfView}>
                      <Text>{data.message}</Text>
                    </View>
                    <View
                      style={{
                        alignSelf: "flex-end",
                      }}
                    >
                      <Text style={styles.ntfTime}>
                        {dateFormatting(new Date(data.time))}
                      </Text>
                    </View>
                  </View>
                  <View style={styles.iconView}>
                    <Image
                      source={require("../../assets/images/loader-icon.png")}
                      style={styles.icon}
                    ></Image>
                  </View>
                </View>
              );
            } else {
              return (
                <View style={styles.adminNtf} key={key}>
                  <View style={styles.iconView}>
                    <Image
                      source={require("../../assets/images/menume-logo.png")}
                      style={styles.icon}
                    ></Image>
                  </View>
                  <View>
                    <View style={styles.ntfView}>
                      <Text>{data.message}</Text>
                    </View>
                    <View
                      style={{
                        alignSelf: "flex-end",
                      }}
                    >
                      <Text style={styles.ntfTime}>
                        {dateFormatting(new Date(data.time))}
                      </Text>
                    </View>
                  </View>
                </View>
              );
            }
          })}
        </ScrollView>
        <View
          style={{
            position: "absolute",
            bottom: 90,
            right: 5,
            zIndex: 9999,
            justifyContent: "center",
            // backgroundColor: "red",
          }}
        >
          <CallWaiter></CallWaiter>
        </View>
      </View>
    );
  }
};

export default NotificationScreen;
