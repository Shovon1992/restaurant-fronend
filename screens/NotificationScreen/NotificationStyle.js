import { StyleSheet } from "react-native";
import Theme from "../../constants/Theme";

const styles = StyleSheet.create({
  container: {
    position: "relative",
    height: Theme.deviceHeight,
    width: Theme.deviceWidth,
  },
  adminNtf: {
    width: Theme.deviceWidth * 0.98,
    // backgroundColor: "red",
    flexDirection: "row",
    alignSelf: "center",
    marginVertical: 5,
    justifyContent: "flex-start",
  },
  userNtf: {
    width: Theme.deviceWidth * 0.98,
    // backgroundColor: "green",
    flexDirection: "row",
    alignSelf: "center",
    marginVertical: 5,
    justifyContent: "flex-end",
  },
  iconView: {
    width: 35,
    // height: 30,
    justifyContent: "flex-start",
    alignContent: "center",
    alignItems: "center",
  },
  ntfView: {
    maxWidth: Theme.deviceWidth * 0.7,
    // backgroundColor: "#f5f6ff",
    backgroundColor: "#f2f3f6",
    borderRadius: 10,
    borderTopLeftRadius: 0,
    padding: 10,
  },
  userNtfView: {
    maxWidth: Theme.deviceWidth * 0.7,
    justifyContent: "flex-end",
    // backgroundColor: "#f5f6ff",
    backgroundColor: "#e6f7e8",
    borderRadius: 10,
    borderTopRightRadius: 0,
    padding: 10,
  },
  icon: {
    width: 25,
    height: 25,
    // borderRadius: "100%",
  },
  ntfTime: {
    color: "#b9babd",
    fontSize: 12,
  },
  waiterButtonView: {
    position: "absolute",
    bottom: 80,
    right: 10,
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
  },

  modal: {
    width: Theme.deviceWidth,
    padding: 0,
    height: Theme.deviceWidth * 0.5,
    margin: 0,
  },
  modalContent: {
    height: 200,
    width: Theme.deviceWidth,
    marginTop: "auto",
    backgroundColor: "#fff",
    position: "relative",
    // paddingTop: 30,
    padding: 10,
    paddingTop: 25,
  },
  modalCloseButton: {
    width: 32,
    height: 40,
    position: "absolute",
    right: 0,
    top: 2,
  },
});

export default styles;
