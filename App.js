import React, { useState, useEffect, useRef } from "react";
/* Push notification imports */
import * as Notifications from "expo-notifications";
import * as Permissions from "expo-permissions";

/* End of Push notification imports */

// import AsyncStorage from 'react-native';
import Navigation from "./navigation/Navigation";
import { enableScreens } from "react-native-screens";
import Amplify, { Auth } from "aws-amplify";
import awsconfig from "./aws-exports";
// import axios from 'axios';
import * as SplashScreen from "expo-splash-screen";

Amplify.configure(awsconfig);

//use for reducers
import { createStore, combineReducers, applyMiddleware } from "redux";
import ReduxThunk from "redux-thunk";
import { Provider } from "react-redux";
/* import meals from the reducer */
import ReducerMeals from "./store/reducers/mealsCategoryReducer";
import ReucerFoodItems from "./store/reducers/foodItemReducer";
import ReducerWelcome from "./store/reducers/welcome";
import AuthReducer from "./store/reducers/authReducer";
import ReducerCart from "./store/reducers/cartReducer";
import InfoReducer from "./store/reducers/InfoReducer";
import OrderReducer from "./store/reducers/orderReducer";
import notificationReducer from "./store/reducers/notificationReducer";

import { AsyncStorage } from "react-native";

//import PersistantLogin from './screens/Auth/PersistantLogin';

enableScreens();

/* using Redux to call reducers from store directory */
const rootReducer = combineReducers({
  auth: AuthReducer,
  info: InfoReducer,
  welcome: ReducerWelcome,
  meals: ReducerMeals,
  foodItems: ReucerFoodItems,
  cart: ReducerCart,
  orders: OrderReducer,
  notification: notificationReducer,
});

const store = createStore(rootReducer, applyMiddleware(ReduxThunk));

/* Push Notification  */
Notifications.setNotificationHandler({
  handleNotification: async () => ({
    shouldShowAlert: true,
    shouldPlaySound: false,
    shouldSetBadge: false,
  }),
});

export default function App() {
  const [appIsReady, setAppisReady] = useState(false);
  const [authenticating, setAuthenticating] = useState(false);
  /* Push Notification constants  */
  const [expoPushToken, setExpoPushToken] = useState("");
  const [notification, setNotification] = useState(false);
  const notificationListener = useRef();
  const responseListener = useRef();

  useEffect(() => {
    registerForPushNotificationsAsync().then((token) =>
      setExpoPushToken(token)
    );

    // This listener is fired whenever a notification is received while the app is foregrounded
    notificationListener.current =
      Notifications.addNotificationReceivedListener((notification) => {
        setNotification(notification);
      });

    // This listener is fired whenever a user taps on or interacts with a notification (works when app is foregrounded, backgrounded, or killed)
    responseListener.current =
      Notifications.addNotificationResponseReceivedListener((response) => {
        // console.log("responseListener", response);
      });

    return () => {
      Notifications.removeNotificationSubscription(notificationListener);
      Notifications.removeNotificationSubscription(responseListener);
    };
  }, []);

  /* End of Push Notification constants  */

  useEffect(() => {
    (async () => {
      try {
        await SplashScreen.preventAutoHideAsync();
      } catch (err) {
        //console.log(err);
      }
      prepareResources();
    })();
  }, []);

  const prepareResources = async () => {
    await performAPICalls();
  };

  const performAPICalls = async () => {
    // try{
    //  //
    // }catch(error){
    //   setAuthStatus(false);
    //   setUser();
    //   console.log('auth status',error);
    // }

    setAppisReady(true);
    await SplashScreen.hideAsync();
  };

  if (!appIsReady) {
    return null;
  }

  return (
    <Provider store={store}>
      <Navigation />
    </Provider>
  );
  /* Async function for push notification permission and token generate */
  async function registerForPushNotificationsAsync() {
    let token;
    const { status: existingStatus } =
      await Notifications.getPermissionsAsync();
    // Permissions.NOTIFICATIONS
    // ();
    // console.log(existingStatus);

    let finalStatus = existingStatus;
    if (existingStatus !== "granted") {
      const { status } = await Notifications.requestPermissionsAsync();
      finalStatus = status;
    }
    if (finalStatus !== "granted") {
      // console.log(
      //   "Failed to get push token for push notification!" + finalStatus
      // );
      return;
    }
    token = (await Notifications.getDevicePushTokenAsync()).data;

    AsyncStorage.setItem("DeviceToken", token);
    // console.log("Device token : ", token);

    // to send a expo token
    /*Notifications.scheduleNotificationAsync({
      content: {
        title: "Demo notification",
        body: "This is demo notification",
      },
      trigger: {
        seconds: 5,
      },
    });*/

    //return token;
  }
}
