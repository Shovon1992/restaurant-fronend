import React, { useState } from "react";
import {
  View,
  Platform,
  Text,
  TextInput,
  ActivityIndicator,
  Image,
  KeyboardAvoidingView,
  ScrollView,
} from "react-native";
import { useDispatch } from "react-redux";
import styles from "../LoginScreen/loginStyle";
import * as authActions from "../../../store/actions/authAction";

import { validateAll } from "indicative/validator";

import Message from "../../../constants/Message";
import CustomButton from "../../../CustomComponents/Button/Button";

import Theam from "../../../constants/Theme";
import DateTimePicker from "@react-native-community/datetimepicker";

import moment from "moment";
import Icon from "react-native-vector-icons/FontAwesome";

const SignupScreen = (props) => {
  const dispatch = useDispatch();

  const [error, setError] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  const [userName, setUserName] = useState("");
  const [password, setPassword] = useState("");
  const [name, setName] = useState("");
  const [phone, setPhone] = useState("");
  const [email, setEmail] = useState("");
  const [dob, setDob] = useState("");
  const [gender, setGender] = useState("");

  //date picker
  const [date, setDate] = useState(new Date());
  const [show, setShow] = useState(false);

  const showDatepicker = () => {
    setShow(true);
  };

  const onChange = (selectedDate) => {
    setShow(Platform.OS === "ios");
    const dob = selectedDate.nativeEvent.timestamp;
    const formatDob = moment(dob).format("DD/MM/YYYY");
    setDob(formatDob);
  };

  var radio_props = [
    { label: "male", value: "male" },
    { label: "female", value: "female" },
  ];

  const state = {
    userName: userName,
    password: password,
    name: name,
    phone: phone,
    email: email,
    // dob: dob,
    // gender: gender
  };

  const signUp = async () => {
    const rules = {
      password: "required|min:6",
      name: "required",
      phone: "required",
      email: "required|email",
      // dob: 'required',
      // gender: 'required'
    };

    const messages = {
      required: (field) => `${field} is required`,
      "password.min": Message.passwordMin,
      "email.email": Message.validEmail,
    };
    try {
      setIsLoading(true);
      setError({});
      const validateForm = await validateAll(state, rules, messages);
      if (validateForm) {
        await dispatch(authActions.signup(name, email, password, phone));
        // await dispatch(authActions.signup(name, email, password, gender, dob, phone));
        // props.navigation.navigate('ConfirmSignup',{userName: email});
        props.navigation.navigate("ConfirmSignup");
      }
    } catch (err) {
      const formattedErrors = {};
      const checkAPIResponse = Array.isArray(err);

      if (checkAPIResponse !== true) {
        formattedErrors["apiError"] = "" + err + "";
      } else {
        err.forEach((error) => (formattedErrors[error.field] = error.message));
      }

      setError(formattedErrors);
      setIsLoading(false);
    }
  };

  return (
    <View style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        {/* <KeyboardAvoidingView behavior='padding'> */}
        <View style={styles.part1}>
          <Image
            source={require("../../../assets/images/menume-logo.png")}
            style={styles.logo}
          ></Image>
        </View>
        <View style={styles.part2}>
          <View style={styles.inputContainer}>
            <Icon
              name="user"
              size={Theam.iconSize}
              style={styles.InputRightIcon}
            />
            <TextInput
              style={styles.textInput}
              placeholder="Name"
              onChangeText={(name) => {
                setName(name);
              }}
            />
          </View>
          {error.name && (
            <View style={styles.errorContainer}>
              <Text style={styles.errorText}>{error.name}</Text>
            </View>
          )}
          <View style={styles.inputContainer}>
            <Icon
              name="envelope"
              size={Theam.iconSize}
              style={styles.InputRightIcon}
            />
            <TextInput
              style={styles.textInput}
              placeholder="Email"
              keyboardType="email-address"
              autoCapitalize="none"
              onChangeText={(email) => {
                setEmail(email);
              }}
            />
          </View>
          {error.email && (
            <View style={styles.errorContainer}>
              <Text style={styles.errorText}>{error.email}</Text>
            </View>
          )}

          <View style={styles.inputContainer}>
            <Icon
              name="phone"
              size={Theam.iconSize}
              style={styles.InputRightIcon}
            />
            <TextInput
              style={styles.textInput}
              placeholder="Phone"
              keyboardType="phone-pad"
              maxLength={10}
              onChangeText={(phone) => {
                setPhone(phone);
              }}
            />
          </View>
          {error.phone && (
            <View style={styles.errorContainer}>
              <Text style={styles.errorText}>{error.phone}</Text>
            </View>
          )}
          <View style={styles.inputContainer}>
            <Icon
              name="lock"
              size={Theam.iconSize}
              style={styles.InputRightIcon}
            />

            <TextInput
              style={styles.textInput}
              placeholder="Password"
              secureTextEntry={true}
              onChangeText={(password) => {
                setPassword(password);
              }}
            />
          </View>
          {error.password && (
            <View style={styles.errorContainer}>
              <Text style={styles.errorText}>{error.password}</Text>
            </View>
          )}

          {isLoading ? (
            <View style={styles.loading}>
              <ActivityIndicator size="large" color="black" />
              <Text style={{ textAlign: "center" }}>Validating</Text>
            </View>
          ) : (
            <View style={styles.buttonWrapper}>
              {/* <View style={styles.bottomLink , {alignSelf:'flex-end'}}>
                            <TouchableOpacity onPress={() => { }}>
                                <Text style={styles.bottomLinkText}>Forgot Password?</Text>
                            </TouchableOpacity>
                        </View> */}
              {error.apiError && (
                <View style={styles.errorContainer}>
                  <Text style={styles.errorText}>{error.apiError}</Text>
                </View>
              )}

              <View
                style={
                  (styles.buttonMainContainer,
                  styles.loginBtn,
                  { marginTop: Theam.deviceHeight * 0.09 })
                }
              >
                <CustomButton
                  onPress={signUp}
                  title="Signup  "
                  style={styles.loginBtnStyle}
                />
              </View>

              {/* <View style={styles.textToOption}>
                            <View style={styles.lineStyle}></View>
                                    <Text style={styles.loginButtonBelowText}> Or Login With</Text>
                            <View style={styles.lineStyle}></View>
                        </View> */}

              {/* <View style={styles.OtherLoginButtonContainer}> 
                            <View style={styles.buttonMainContainer}>
                               
                                <CustomButton 
                                        onPress={()=>{}} 
                                        title='Facebook' 
                                        style={styles.FacebookButtonStyle} 
                                        IconButtonStyle={styles.IconButtonStyle}
                                        buttonText={styles.SocialbuttonText}
                                >
                                    <Ionicons
                                        name='logo-facebook'
                                        size={Theam.deviceHeight*.05}
                                        style ={{...styles.InputRightIcon, color : 'white'}}
                                    />
                                </CustomButton>
                            </View>
                            <View style={styles.buttonMainContainer}>
                                <CustomButton 
                                        onPress={()=>{}} 
                                        title='Google' 
                                        style={styles.GoogleButtonStyle} 
                                        IconButtonStyle={styles.IconButtonStyle}
                                        buttonText={styles.SocialbuttonText}
                                        disabled={true}
                                >
                                    <Ionicons
                                        name='logo-google'
                                        size={Theam.deviceHeight*.05}
                                        style ={{...styles.InputRightIcon, color : 'white'}}
                                    />
                                </CustomButton>
                            </View>
                        </View> */}
            </View>
          )}
        </View>
        {/* </KeyboardAvoidingView> */}
      </ScrollView>
    </View>
  );
};

SignupScreen.navigationOptions = {
  headerTransparent: true,
  headerTitle: "",
};

export default SignupScreen;
