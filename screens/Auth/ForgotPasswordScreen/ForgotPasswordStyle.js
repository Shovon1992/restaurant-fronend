import { StyleSheet } from "react-native";
import Theme from "../../../constants/Theme";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: Theme.background,
  },
});
export default styles;
