export const ADD_ORDER = "ADD_ORDER";
export const FETCH_ORDER = "FETCH_ORDER";
import config from "../../constants/config";
import Order from "../../model/orders";

export const fetchOrderAPI = () => {
  return async (dispatch, getState) => {
    const userId = getState().auth.userId;

    try {
      const response = await fetch(config.order +"/"+ userId, {
        method: "GET",
        headers: config.headers,
      });

      const resData = await response.json();
      console.log('resData fetch Order',config.order + userId,resData);

      let orderDetails = [];
      let totalAmount = 0;
      for (const key in resData["data"]) {
        orderDetails.push({
          id: resData["data"][key]["orderId"],
          restaurant: resData["data"][key]["restaurantName"],
          table: resData["data"][key]["tableId"],
          quantity: resData["data"][key]["qty"],
          tableCode: resData["data"][key]["tableCode"],
          image: resData["data"][key]["foodItemThumbImage"],
        });
        totalAmount +=
          resData["data"][key]["price"] *
          resData["data"][key]["qty"];
      }
      dispatch({
        type: FETCH_ORDER,
        orders: orderDetails,
        totalAmount: totalAmount,
      });
    } catch (error) {
      // console.log(error);
    }
  };
};

export const fetchOrderDetails = (orderId) => {
  return async (dispatch, getState) => {
    try {
      const response = await fetch(config.orderDetails + "/"+orderId, {
        method: "GET",
        headers: config.headers,
      });

      const resData = await response.json();

      console.log('fetchOrderDetails',resData);

      let orderDetails = [];
      let totalAmount = 0;
      for (const key in resData["data"]) {
        orderDetails.push({
          id: resData["data"][key]["orderId"],
          item: resData["data"][key]["itemName"],
          price: resData["data"][key]["price"],
          quantity: resData["data"][key]["qty"],
          tableCode: resData["data"][key]["tableCode"],
          image: resData["data"][key]["foodItemThumbImage"],
        });
        totalAmount +=
          resData["data"][key]["price"] *
          resData["data"][key]["qty"];
      }
      dispatch({
        type: FETCH_ORDER,
        orders: orderDetails,
        totalAmount: totalAmount,
      });
    } catch (error) {
      // console.log(error);
    }
  };
};

export const addOrder = (cartItems, totalAmount) => {
  return async (dispatch) => {
    const date = new Date();
    console.log('addOrder cartItems',cartItems);
    try {
      // console.log('cartId',cartItems[0].cartId)
      const response = await fetch(config.order, {
        method: "POST",
        headers: config.headers,
        body: JSON.stringify({
          cartId: cartItems[0].cartId,
          // totalprice : '',
          // listPrice : '',
          // discount :'',
          // CuponCode : '',
          // tax :'',
          // transtionMethod : '',
          // trnsactionId : '',
          userMessage: "elu",
        }),
      });

      const resData = await response.json();

       console.log('addOrder-----------------------------------------0000000000000000',resData);

      dispatch({
        type: ADD_ORDER,
        orderData: {
          id: resData.data["hash_id"],
          items: cartItems,
          amount: totalAmount,
          date: date,
        },
        orderId: resData.data["hash_id"],
      });
    } catch (error) {
      // console.log('error',error)
    }
  };
};
