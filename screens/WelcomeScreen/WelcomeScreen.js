import React, { useState, useEffect } from "react";
import { View, Text, Button } from "react-native";
import Carousel from "../../CustomComponents/Carousel/Carousel";
import { StatusBar } from "react-native";
import styles from "./WelcomeScreenStyle";
import { useDispatch } from "react-redux";
import * as welcomeActions from "../../store/actions/welcomAction";
import * as AuthActions from "../../store/actions/authAction";
/* get data from the reducers using redux */
import { useSelector } from "react-redux";
import Welcome from "../../model/welcome";
import Loader from "../../CustomComponents/Loader/loader";

const WelcomeScreen = (props) => {
  const dispatch = useDispatch();
  const [isLoading, setIsLoading] = useState(false);
  //let welcomeData =  useSelector(state=> state.welcome.data)
  welcomeData = useSelector((state) => state.welcome.data);
  selectedTable = useSelector((state) => state.info.selectedTable);
  const restaurantId = props.navigation.getParam("id");

  // const restaurantId = 'fdd3c752480227cb3f5f257809918e25599d3e68';
  // const tableNo = 13;
  const welcomeDummyData = [
    new Welcome(
      "https://images.unsplash.com/photo-1517248135467-4c7edcad34c4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80",
      "Welome to Restaurant",
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,"
    ),
  ];

  //     image: 'https://images.unsplash.com/photo-1517248135467-4c7edcad34c4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80',
  //     header : 'Welome to Restaurant',
  //     description : 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,'

  // }];
  let welcomeData = useSelector((state) => state.welcome.data);
  useEffect(() => {
    const getWelcomeData = async () => {
      setIsLoading(true);
      await dispatch(
        welcomeActions.welcomescreen(selectedTable.id, selectedTable.table)
      );
      setIsLoading(false);
    };
    getWelcomeData();
  }, [dispatch]);

  //let displayData = (welcomeData)? welcomeData : welcomeDummyData;
  if (isLoading) {
    return <Loader></Loader>;
  } else {
    let displayData =
      welcomeData && welcomeData[0] ? welcomeData : welcomeDummyData;
    return (
      <View style={styles.container}>
        <StatusBar
          barStyle="light-content"
          backgroundColor="rgba(154, 248, 164, 0.05)"
          hidden={false}
          translucent={true}
        />
        {
          <Carousel
            style="slides"
            itemsPerInterval={1}
            items={displayData}
            navigation={props.navigation}
            navigateTo="Category"
            restaurantId={restaurantId}
          />
        }
      </View>
    );
  }
};

const showSlide = (data) => {};
WelcomeScreen.navigationOptions = {
  headerTitle: "",
  headerShown: false,
};

export default WelcomeScreen;
