import React, { useState, useEffect, useCallback } from "react";
import {
  Text,
  View,
  StyleSheet,
  Button,
  Image,
  TouchableOpacity,
  ScrollView,
  AsyncStorage,
} from "react-native";
import { BarCodeScanner } from "expo-barcode-scanner";

import styles from "./QRCodeScannerStyle";

import { useSelector, useDispatch } from "react-redux";
import * as InfoAction from "../../store/actions/InfoAction";
import Theme from "../../constants/Theme";
const QRCodeScannerScreen = (props) => {
  const dispatch = useDispatch();
  const [hasPermission, setHasPermission] = useState(null);
  const [scanned, setScanned] = useState(false);
  const [prevRestaurant, setPrevRestaurant] = useState(null);

  useEffect(() => {
    (async () => {
      const { status } = await BarCodeScanner.requestPermissionsAsync();
      setHasPermission(status === "granted");

      var prevScans = await AsyncStorage.getItem("scanTables");
      if (prevScans != null) {
        prevScans = JSON.parse(prevScans);
        setPrevRestaurant(prevScans);
      }
    })();
  }, []);

  const selectTable = useCallback(
    async (table) => {
      try {
        await dispatch(InfoAction.selectTable(table));
        props.navigation.navigate("Welcome");
      } catch (error) {}
    },
    [dispatch]
  );

  const handleBarCodeScanned = ({ type, data }) => {
    setScanned(true);
    const restaurantData = data.split(",");
    if (restaurantData && restaurantData[0].trim() == "MenueMe") {
      var tabNo = "T1";
      if (restaurantData[4] != null) {
        tabNo = restaurantData[4];
      }
      selectTable({
        id: restaurantData[1],
        restaurantName: restaurantData[2],
        table: restaurantData[3],
        tableNumber: tabNo,
      });
    } else {
      alert("Please Scan Valid QR Code !!");
    }
  };

  if (hasPermission === null) {
    return <Text>Requesting for camera permission</Text>;
  }
  if (hasPermission === false) {
    return <Text>No access to camera</Text>;
  }

  return (
    <View style={styles.screen}>
      <BarCodeScanner
        onBarCodeScanned={scanned ? undefined : handleBarCodeScanned}
        style={[styles.container]}
      >
        <Text style={styles.description}>Scan QR code</Text>

        <Image
          style={styles.qr}
          source={require("../../assets/images/QR.png")}
        />
        {console.log(prevRestaurant)}
        {prevRestaurant && (
          <View style={styles.prevScansView}>
            <ScrollView
              horizontal={true}
              showsHorizontalScrollIndicator={false}
            >
              {prevRestaurant.map((rest, index) => {
                return (
                  <TouchableOpacity
                    key={index}
                    style={styles.prevScanRest}
                    onPress={() => {
                      selectTable({
                        id: rest.restaurantId,
                        restaurantName: rest.restaurantName,
                        table: rest.tableId,
                        tableNumber: rest.tableNumber,
                      });
                    }}
                  >
                    <View
                      style={{
                        borderRadius: 100,
                        width: 50,
                        height: 50,
                        justifyContent: "center",
                        alignItems: "center",
                        backgroundColor: Theme.greenColor,
                      }}
                    >
                      <Text style={{ color: "#fff" }}>{rest.tableId}</Text>
                    </View>
                    <View>
                      <Text
                        style={{ color: Theme.accentColor }}
                        numberOfLines={1}
                      >
                        {rest.restaurantName}
                      </Text>
                    </View>
                  </TouchableOpacity>
                );
              })}
            </ScrollView>
          </View>
        )}
      </BarCodeScanner>
      {scanned && (
        <Button title={"Tap to Scan Again"} onPress={() => setScanned(false)} />
      )}
    </View>
  );
};

QRCodeScannerScreen.navigationOptions = {
  headerTitle: "",
  headerTransparent: true,
  headerShown: false,
};

export default QRCodeScannerScreen;
