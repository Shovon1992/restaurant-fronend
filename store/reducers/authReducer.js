import {
  SIGNUP,
  SIGNIN,
  CONFIRMSIGNUP,
  AUTOLOGIN,
  LOGOUT,
} from "../actions/authAction";

const inititalState = {
  jwtToken: null,
  userId: null,
  userData: {},
  // name : null,
  // email: null,
  // phone : null,
  // dob: null,
  // gender: null
};

export default (state = inititalState, action) => {
  switch (action.type) {
    case SIGNIN:
      return {
        jwtToken: action.jwtToken,
        userId: action.userId,
        userData: {
          name: action.name,
          email: action.email,
          phone: action.phone,
          // dob: action.dob,
          // gender: action.gender
        },
      };
    case SIGNUP:
      return {
        email: action.email,
      };
    case AUTOLOGIN:
      return {
        jwtToken: action.token,
        userId: action.userId,
      };
    case LOGOUT:
      return inititalState;

    default:
      return state;
  }
};
