import { StyleSheet } from "react-native";
import Theme from "../../../constants/Theme";

const styles = StyleSheet.create({
  container: {
    width: Theme.deviceWidth,
    // backgroundColor: "red",
    alignSelf: "center",
    // flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  imageView: {
    flexDirection: "row",
    alignSelf: "center",
    height: Theme.deviceHeight * 0.3,

    // marginBottom: 10,
  },
  logo: {
    borderRadius: 100,
    width: Theme.deviceHeight * 0.2,
    height: Theme.deviceHeight * 0.2,
    alignSelf: "flex-start",
    top: Theme.deviceHeight * 0.05,
  },
  IconView: {
    width: 40,
    justifyContent: "flex-start",
    alignItems: "flex-end",
    // backgroundColor: "red",
    paddingVertical: 15,
  },
  infoView: {
    width: Theme.deviceWidth * 0.85,

    overflow: "hidden",
    borderBottomWidth: 1.5,
    borderBottomColor: Theme.accentColor,
    paddingBottom: 20,
    marginBottom: 20,
  },
  heading: {
    fontSize: Theme.headerTextSize * 0.85,
    fontWeight: "bold",
    lineHeight: 40,
    // textTransform: "capitalize",
  },
  headingLeftIcon: {
    marginRight: 20,
  },
  formView: {
    width: Theme.deviceWidth * 0.8,

    paddingBottom: 20,
    marginBottom: 20,
  },
  inputView: {
    marginVertical: 10,
  },
  textInput: {
    padding: 10,
  },
  buttonView: {
    marginVertical: 20,
  },
});

export default styles;
