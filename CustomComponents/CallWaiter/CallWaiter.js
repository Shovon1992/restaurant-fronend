import React, { useCallback, useState } from "react";
import {
  View,
  Text,
  ScrollView,
  Image,
  TouchableOpacity,
  TextInput,
  Button,
  ToastAndroid,
} from "react-native";
import Loader from "../../CustomComponents/Loader/loader";
import Modal from "react-native-modal";
import Icon from "react-native-vector-icons/Ionicons";
import { useDispatch, useSelector } from "react-redux";
import Theme from "../../constants/Theme";
import CustomButton from "../Button/Button";
import styles from "./CallWaiterStyle";
import * as notificationAction from "../../store/actions/notification";

const CallWaiter = (props) => {
  const dispatch = useDispatch();
  const [isLoading, setIsLoading] = useState(false);

  const restaurantId = useSelector((state) => state.info.selectedTable.id);
  const table = useSelector((state) => state.info.selectedTable);
  const tableNo = table.table;
  const userId = useSelector((state) => state.auth.userId);

  const [modalVisible, setModalVisible] = useState(false);
  const [message, setMessage] = useState("");

  const messages = [
    { title: "Need Water" },
    { title: "Need Salt" },
    { title: "Need Tissue Paper" },
  ];

  const sendNotification = useCallback(async () => {
    setModalVisible(false);
    setIsLoading(true);
    try {
      await dispatch(
        notificationAction.notificationAdd(
          restaurantId,
          userId,
          message,
          tableNo
        )
      );

      setIsLoading(false);
      ToastAndroid.show(
        "Request recieved, Waiter will be at your service in a moment!",
        ToastAndroid.SHORT
      );
      setMessage("");
    } catch (error) {
      console.log("Error : ", error);
      setIsLoading(false);
    }
  });

  if (isLoading) {
    return <Loader></Loader>;
  } else {
    return (
      <View>
        <TouchableOpacity
          style={styles.waiterButtonView}
          onPress={() => {
            setModalVisible(true);
          }}
        >
          <View style={styles.waiterButtonIconView}>
            <Icon name="chatbox" size={Theme.iconSize} color={"#fff"} />
          </View>
          <View style={styles.waiterButtonTextView}>
            <Text style={{ color: "#fff" }}> Call Waiter</Text>
          </View>
        </TouchableOpacity>
        <Modal
          animationType="fade"
          isVisible={modalVisible}
          transparent={true}
          // animationInTiming={900}
          // animationOutTiming={900}
          // backdropTransitionInTiming={900}
          // backdropTransitionOutTiming={900}
          mode="overFullScreen"
          coverScreen={true}
          style={styles.modal}
          onBackButtonPress={() => setModalVisible(false)}
          onBackdropPress={() => {
            setModalVisible(false);
          }}
        >
          <View style={styles.modalContent}>
            <TouchableOpacity
              style={styles.modalCloseButton}
              onPress={() => {
                setModalVisible(false);
              }}
            >
              <Icon
                name="close"
                size={Theme.iconSize}
                color={Theme.primaryColor}
              />
            </TouchableOpacity>
            <View style={styles.messageView}>
              <ScrollView
                horizontal={true}
                showsHorizontalScrollIndicator={false}
              >
                {messages.map((msg, index) => {
                  return (
                    <TouchableOpacity
                      key={index}
                      style={{
                        // borderWidth: 1,
                        // borderColor: Theme.grayColor,
                        backgroundColor: "#e6f7e8",
                        borderRadius: 10,
                        paddingHorizontal: 10,
                        paddingVertical: 10,
                        marginRight: 5,
                        justifyContent: "center",
                      }}
                      onPress={() => {
                        setMessage(msg.title);
                      }}
                    >
                      <Text>{msg.title}</Text>
                    </TouchableOpacity>
                  );
                })}
              </ScrollView>
            </View>
            <View style={styles.customMessageView}>
              <View style={styles.messageInputView}>
                <TextInput
                  placeholder="Custom message"
                  borderWidth={1}
                  borderColor={Theme.grayColor}
                  style={styles.messageInput}
                  value={message}
                  onChangeText={(value) => {
                    setMessage(value);
                  }}
                />
              </View>
              <View style={styles.buttonView}>
                <CustomButton
                  onPress={sendNotification}
                  title="Send"
                  disabled={!message}
                ></CustomButton>
              </View>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
};

export default CallWaiter;
