import React, { useState, useEffect,useCallback } from 'react';
import styles from './CartStyle';
import { View, Text, ImageBackground, FlatList, TextInput, ScrollView } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';

import { useSelector, useDispatch } from 'react-redux';
import { Ionicons } from '@expo/vector-icons';
import Loader from '../../CustomComponents/Loader/loader';

import * as cartActions from '../../store/actions/cartAction';
import * as orderActions from '../../store/actions/orderAction';
/** Main part to  */
const CartScreen = props => {
    const dispatch = useDispatch();
    const [isLoading, setIsLoading] = useState(false);
    const [isRefreshing, setIsRefreshing] = useState(false);

   
    const cartItems = useSelector(state => {
        const transformedCartItems = [];
        for (const key in state.cart.items) {
            transformedCartItems.push({
                id: key,
                item: state.cart.items[key].item,
                price: state.cart.items[key].price,
                quantity: state.cart.items[key].quantity,
                image: state.cart.items[key].image,
                sum: state.cart.items[key].sum,
            });
        }
        return transformedCartItems.sort(
            (a, b) => a.id > b.id ? 1 : -1
        );

    })
    const cartTotal = useSelector(state => state.cart.totalAmount);

    /* Place Order Button Handler */
    const orderHandler = async () => {
        try {
            await dispatch(orderActions.addOrder(cartItems, cartTotal));
            props.navigation.navigate('Thankyou');
        } catch (error) {
            console.log(error);
        }
        
    }


    const cartLists = itemData => {
        console.log('itemData',itemData);
        return (
            <View style={styles.gridItems}  >

                <ImageBackground source={{ uri: itemData.item.image }} style={styles.itemImage}></ImageBackground>

                <View style={styles.itemInfo}>
                    <View>
                        <Text style={styles.itemHeader}>
                            {itemData.item.item}
                        </Text>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <TouchableOpacity onPress={() => { dispatch(cartActions.addQtyToCart(itemData.item.id)) }}>
                            <Ionicons
                                name="md-add"
                                size={25}
                                style={styles.quantityButton}

                            ></Ionicons>
                        </TouchableOpacity>
                        <View style={{ paddingHorizontal: 20 }}>
                            <Text style={{ fontWeight: 'bold', fontSize: 20 }}>
                                {itemData.item.quantity}   </Text>
                           
                            
                        </View>
                        <TouchableOpacity onPress={() => { dispatch(cartActions.removeQtyFromCart(itemData.item.id)) }}>
                            <Ionicons
                                name="md-remove"
                                size={25}
                                style={styles.quantityButton}

                            ></Ionicons>
                        </TouchableOpacity>
                    </View>

                </View>

                <View style={styles.itemAction}>
                    <View>
                        <Text style={styles.itemPrice}>
                            ${itemData.item.sum.toFixed(2)}   </Text>
                    </View>
                </View>
            </View>
        )
    }

    if (isLoading) {
        return (<Loader></Loader>)
    } else {
        if (cartItems.length !== 0) {

            return (

                <View style={{ flex: 1 }} >

                    <FlatList
                        onRefresh={loadProducts}
                        refreshing={isRefreshing}
                        // keyExtractor={(item,index)=>item.id}
                        data={cartItems}
                        renderItem={cartLists}
                        numColumns={1}

                    />
                    <ScrollView>
                        <View style={styles.summarySection}>
                            <View style={styles.sugesstionContainer}>
                                <Text style={{ paddingBottom: 5 }}>Suggestion for the Cook: </Text>
                                <TextInput
                                    style={styles.TextInputStyleClass}
                                    underlineColorAndroid="transparent"
                                    numberOfLines={3}
                                    multiline={true}
                                />
                            </View>

                            <View style={styles.itemSummary}>
                                <View style={{ ...styles.commonItems }}>
                                    <Text style={styles.commonText}>Item Total  </Text>
                                    <Text style={styles.commonText}>${cartTotal}  </Text>
                                </View>
                                <View style={{ ...styles.commonItems }}>
                                    <Text style={styles.commonText}>Coupon Discount   </Text>
                                    <Text style={{ ...styles.totalPrice, ...styles.commonText }}>APPLIED  </Text>
                                </View>
                                <View style={{ ...styles.commonItems }}>
                                    <Text style={styles.commonText}>Delivery Charge   </Text>
                                    <Text style={styles.commonText}>free  </Text>
                                </View>
                            </View>

                            <View style={styles.TotalConatainer}>
                                <Text style={styles.itemHeader}>Total </Text>
                                <Text style={{ ...styles.itemHeader, ...styles.totalPrice }}>${cartTotal}  </Text>
                            </View>
                        </View>
                    </ScrollView>
                    <View style={styles.proceedBtn}>
                        <TouchableOpacity onPress={orderHandler}>
                            <Text style={styles.proceedText}>Proceed to Checkout</Text>
                        </TouchableOpacity>
                    </View>
                </View>



            )
        }
        else {
            return (
                <View>
                    <View style={{ flex: 1, top: 150 }}>
                        <Ionicons
                            name="md-sad"
                            size={100}
                            style={{ alignSelf: 'center', color: 'gray' }}
                        ></Ionicons>
                        <Text style={{ textAlign: 'center', color: 'gray', fontSize: 25 }}>
                            No Items added on cart
                        </Text>
                    </View>
                </View>

            )
        }
    }
}
CartScreen.navigationOptions = {
    headerTitle: 'My Cart '
}
export default CartScreen;



