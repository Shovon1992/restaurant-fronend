import {WELCOMESCREEN} from '../actions/welcomAction';

const initialState = {
    data : null,
    restrauntInfo : {}
}

export default (state = initialState, action) => {
    switch(action.type){
        case WELCOMESCREEN :
            return { 
                data : action.data,
                restrauntInfo : action.restrauntInfo
            }
        default : 
        return state;
     }
}
 