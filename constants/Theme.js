import { Dimensions } from "react-native";

export default {
  background: "#FCFDDA",
  primaryColor: "#4a148c",
  accentColor: "#ff6f00",
  headerTextColor: "black",
  greenColor: "#79BF34",
  registerButton: "#FCFDDA",
  grayColor: "#696560",
  textSize: 15,
  subheader: 12,
  headerTextSize: 20,
  deviceHeight: Dimensions.get("window").height,
  deviceWidth: Dimensions.get("window").width,
  bottonText: 15,
  iconSize: 25,
};
