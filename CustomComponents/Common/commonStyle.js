import { StyleSheet } from "react-native";
import Theme from "../../constants/Theme";
const styles = StyleSheet.create({
  buttonMainContainer: {
    padding: 20,
  },
  button: {
    width: Theme.deviceWidth * 0.75,
    height: 50,
    borderRadius: 5,
    justifyContent: "center",
    alignSelf: "center",
  },
});

export default styles;
