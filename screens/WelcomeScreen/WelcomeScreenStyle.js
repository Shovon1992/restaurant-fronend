import { StyleSheet } from 'react-native';
import Theme from '../../constants/Theme';
const styles = StyleSheet.create({
    container : {
        flex : 1,
        alignItems : 'center',
        justifyContent : 'center',
        backgroundColor : Theme.background
    },
    styleStatusBar :{
        
    },
    
});

export default styles;