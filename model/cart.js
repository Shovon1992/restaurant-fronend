export default class Cart {
    constructor(item,quantity,price,image,sum,foodItemId,priceId,cartId){
        this.item = item,
        this.quantity = quantity,
        this.price = price,
        this.image = image,
        this.sum = sum,
        this.foodItemId = foodItemId,
        this.priceId = priceId,
        this.cartId = cartId
    }
}