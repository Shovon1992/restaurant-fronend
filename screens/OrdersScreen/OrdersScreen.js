import React, {useState,useCallback,useEffect} from 'react';
import {View,Text,FlatList, TouchableWithoutFeedback} from 'react-native';
import styles from './OrdersStyle';
import { useSelector, useDispatch } from 'react-redux';
import { Ionicons } from '@expo/vector-icons';
import Loader from '../../CustomComponents/Loader/loader';
import CustomButton from '../../CustomComponents/Button/Button';
import {HeaderButtons, Item} from 'react-navigation-header-buttons';
import * as orderActions from '../../store/actions/orderAction';
import customHeaderButton from '../../CustomComponents/HeaderButton/HeaderButton';


const OrdersScreen = props => {
    const dispatch = useDispatch();
    const [isLoading, setIsLoading] = useState(false);
    const [isRefreshing, setIsRefreshing] = useState(false);

    const orderedItems =  useSelector(state => state.orders.orders);

    const fetchOrders = useCallback( async () => {
        setIsLoading(true);
        setIsRefreshing(true);
        try{
           await dispatch(orderActions.fetchOrderAPI());
        }catch (err) {
           //console.log(err);
        }
        setIsRefreshing(false);
        setIsLoading(false);
    },[dispatch,setIsLoading,setIsRefreshing])
    
    /* rerender all the orders from the API  */
    /* when navigating through navigations refetch data through API */
    useEffect(()=>{
        const willFocusSub = props.navigation.addListener('willFocus', fetchOrders);
        //unmount the data , clean up the listner
        return ()=>{
            willFocusSub.remove();
        }
    },[fetchOrders]);

      /* Resist Rerender for loading products */
    useEffect(()=>{
        fetchOrders()
    }, [dispatch,fetchOrders]);


    const orderedItemList = itemData => {
        //console.log('dsfsdfdfdsf',itemData)
        return ( 
            <TouchableWithoutFeedback onPress={() => {
                props.navigation.navigate('Thankyou',{orderId : itemData.item.id})}}
            >
            <View style={styles.screen}>
                <View style={{...styles.section1, ...styles.commonSection}}>
                    <Text style={styles.orderId}>Order ID - <Text>{itemData.item.id}  </Text>
                        <Text style={styles.statusText}> (Deliver Status)           </Text>
                    </Text>
                    <Text style={{textAlign : 'left', right : 50 }}>{itemData.item.quantity} item</Text>
                </View>
                <View style={{...styles.section2, ...styles.commonSection}}>
                <Text style={{...styles.commonText}}>Restaurant - {itemData.item.restaurant}</Text> 
                <Text style={{...styles.commonText}}>Table - {itemData.item.tableCode}</Text> 
                    <Text style={{...styles.commonText}}>Order Total Amount - {(itemData.item.price * itemData.item.quantity).toFixed(2)}</Text> 
                </View>
                <View style={{...styles.section3, ...styles.commonSection}}>
                       <Text style={{...styles.commonText}}>{itemData.item.readableDate}  </Text>
                    <CustomButton 
                            title='reorder  ' 
                            style={styles.buttonStyle} 
                            buttonText={styles.buttonText}
                            onPress={()=>{}}
                    />
                </View>
            </View>
            </TouchableWithoutFeedback>
        )
    }

    if (isLoading) {
        return (<Loader></Loader>)
    } else {
        //console.log('eeeeeeee',orderedItemList)
        if (orderedItems.length !==0) {
    return(
                
                <FlatList
                    onRefresh={fetchOrders}
                    refreshing={isRefreshing}
                    data={orderedItems}
                    renderItem={orderedItemList}
                    numColumns={1}
                />
               
               
               
            )
        }
        else {
            return (
                <View>
                    <View style={{ flex: 1, top: 150 }}>
                        <Ionicons
                            name="md-sad"
                            size={100}
                            style={{ alignSelf: 'center', color: 'gray' }}
                        ></Ionicons>
                        <Text style={{ textAlign: 'center', color: 'gray', fontSize: 25 }}>
                            No Orders
                        </Text>
                    </View>
                </View>

            )
        }
}}
OrdersScreen.navigationOptions = navData => {
    return {
        headerTitle : 'Orders',
        headerLeft: ()=> 
        <HeaderButtons HeaderButtonComponent={customHeaderButton}>
            <Item
                title='Menu'
                iconName='ios-menu'
                onPress={()=>{
                    navData.navigation.toggleDrawer()
                }}
            />
        </HeaderButtons>
    }
}
export default OrdersScreen;