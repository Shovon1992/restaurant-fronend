import {
  ADD_TO_CART,
  ADD_QTY_TO_CART,
  REMOVE_QTY_FROM_CART,
  EMPTY_CART,
  FETCH_CART,
} from "../actions/cartAction";

import Cart from "../../model/cart";

const initialState = {
  items: {},
  totalAmount: 0,
};

export default (state = initialState, action) => {
  switch (action.type) {
    /* Fetch cart Items */
    case FETCH_CART:
      return {
        ...state,
        items: action.items,
        totalAmount: state.totalAmount + action.items[1].price,
      };
    //Add Items to the cart
    case ADD_TO_CART:
      const addedProduct = action.product;
      const prodPrice = addedProduct.price;
      const prodTitle = addedProduct.name;
      const imageUrl = addedProduct.thumbUrl;

      console.log("addedProduct", addedProduct);

      let updatedOrNewCartItem;

      if (state.items[addedProduct.id]) {
        updatedOrNewCartItem = new Cart(
          prodTitle,
          state.items[addedProduct.id].quantity + 1,
          prodPrice,
          imageUrl,
          state.items[addedProduct.id].sum + prodPrice
        );
      } else {
        updatedOrNewCartItem = new Cart(
          prodTitle,
          1,
          prodPrice,
          imageUrl,
          prodPrice
        );
      }

      return {
        ...state,
        items: { ...state.items, [addedProduct.id]: updatedOrNewCartItem },
        totalAmount: state.totalAmount + prodPrice,
      };

    //Add Quantity from the cart

    case ADD_QTY_TO_CART:
      const AddselectedCartItem = state.items[action.pid];

      let AddupdatedCartItems;

      const updatedCartItem = new Cart(
        AddselectedCartItem.item,
        parseInt(AddselectedCartItem.quantity) + 1,
        AddselectedCartItem.price,
        AddselectedCartItem.image,
        AddselectedCartItem.sum + AddselectedCartItem.price
      );
      AddupdatedCartItems = { ...state.items, [action.pid]: updatedCartItem };

      return {
        ...state,
        items: AddupdatedCartItems,
        totalAmount: state.totalAmount + AddselectedCartItem.price,
      };

    //Remove Quantity from the cart
    case REMOVE_QTY_FROM_CART:
      const selectedCartItem = state.items[action.pid];

      const currentQty = selectedCartItem.quantity;
      let updatedCartItems;
      if (currentQty > 1) {
        const updatedCartItem = new Cart(
          selectedCartItem.item,
          parseInt(selectedCartItem.quantity) - 1,
          selectedCartItem.price,
          selectedCartItem.image,
          selectedCartItem.sum - selectedCartItem.price
        );
        updatedCartItems = { ...state.items, [action.pid]: updatedCartItem };
      } else {
        updatedCartItems = { ...state.items };
        delete updatedCartItems[action.pid];
      }
      return {
        ...state,
        items: updatedCartItems,
        totalAmount: state.totalAmount - selectedCartItem.price,
      };

    case EMPTY_CART:
      return initialState;
  }
  return state;
};
