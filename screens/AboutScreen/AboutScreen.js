import React from "react";
import { View, Text, Image, ScrollView } from "react-native";

import styles from "./AboutStyle";

import { HeaderButtons, Item } from "react-navigation-header-buttons";
import customHeaderButton from "../../CustomComponents/HeaderButton/HeaderButton";
import Theme from "../../constants/Theme";

const AboutScreen = (props) => {
  return (
    <View style={styles.container}>
      <ScrollView
        style={{
          width: Theme.deviceWidth,
          paddingHorizontal: 15,
        }}
      >
        <View style={styles.imageView}>
          <Image
            source={require("../../assets/images/menume-logo.png")}
            style={styles.logo}
          ></Image>
        </View>
        <View style={styles.infoView}>
          <Text style={styles.heading}>Who are we ? </Text>
          <Text>
            Launched in 2021, Our technology platform connects customers,
            restaurant partners and delivery partners, serving their multiple
            needs. Customers use our platform to search and discover
            restaurants, read and write customer generated reviews and view and
            upload photos, order food delivery, book a table and make payments
            while dining-out at restaurants
          </Text>
        </View>
      </ScrollView>
    </View>
  );
};

AboutScreen.navigationOptions = (navData) => {
  return {
    headerTitle: "About Us",
    headerLeft: () => (
      <HeaderButtons HeaderButtonComponent={customHeaderButton}>
        <Item
          title="Menu"
          iconName="ios-menu"
          onPress={() => {
            navData.navigation.toggleDrawer();
          }}
        />
      </HeaderButtons>
    ),
  };
};

export default AboutScreen;
