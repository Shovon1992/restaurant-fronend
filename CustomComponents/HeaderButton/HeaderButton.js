import React from 'react';
import {HeaderButton} from 'react-navigation-header-buttons';

import {Ionicons} from '@expo/vector-icons';

import Theme from '../../constants/Theme';

const customHeaderButton = props =>{
    return <HeaderButton 
        {...props} 
        IconComponent={Ionicons} 
        iconSize={23} 
        color={Theme.headerTextColor}
    />
}

export default customHeaderButton;