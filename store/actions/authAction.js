import { Auth } from "aws-amplify";
import { AsyncStorage } from "react-native";
import config from "../../constants/config";

export const SIGNUP = "SIGNUP";
export const CONFIRMSIGNUP = "CONFIRMSIGNUP";
export const SIGNIN = "SIGNIN";

export const AUTOLOGIN = "AUTOLOGIN";
export const LOGOUT = "LOGOUT";

//export const signup = (name,email,password,gender,birthdate,phone_number) => {
export const signup = (name, email, password, phone_number) => {
  return async (dispatch) => {
    try {
      const response = await Auth.signUp({
        username: email,
        password: password,
        attributes: {
          name: name,
          // gender: gender,
          // birthdate: birthdate, //'13/03/1992'
          email: email, // optional
          phone_number: "+91" + phone_number, // optional - E.164 number convention '+919476132302'
          // other custom attributes
        },
      });

      //   console.log("signup response is", response);
      dispatch({
        type: SIGNUP,
        email: response.user.username,
      });
    } catch (error) {
      throw new Error(error.message);
    }
  };
};

export const confirm = (username, code) => {
  return async (dispatch) => {
    try {
      const response = await Auth.confirmSignUp(username, code);

      dispatch({
        type: CONFIRMSIGNUP,
      });
    } catch (error) {
      throw new Error(error.message);
    }
  };
};

export const login = (
  username,
  password,
  uuid,
  deviceType,
  deviceName,
  deviceToken
) => {
  return async (dispatch) => {
    try {
      const response = await Auth.signIn(username, password);

      console.log('user login response====',response);

      if (response.username) {
        try {
          const getUser = await fetch(config.advanceLogin, {
            method: "POST",
            headers: config.headers,
            body: JSON.stringify({
              user_id: response.username,
              uuid: uuid,
              device_type: deviceType,
              device_name: deviceName,
              token: deviceToken,
            }),
          });
          const getuserReponse = await getUser.json();
          console.log('user login response299999999999',getuserReponse);
          if (getuserReponse.data[0] && getuserReponse.data[0].hash_id) {
            dispatch({
              type: SIGNIN,
              userId: getuserReponse.data[0].hash_id,
              userData: {
                name: getuserReponse.data[0].name,
                email: getuserReponse.data[0].email,
                phone: getuserReponse.data[0].phone,
                // dob: getuserReponse.data[0].dob,
                // gender: getuserReponse.data[0].gender,
              },
              jwtToken: response.signInUserSession.accessToken.jwtToken,
            });
            saveDataToStorage(
              response.signInUserSession.accessToken.jwtToken,
              getuserReponse.data[0].hash_id
            );
          } else {
            throw new Error("User not found. Please signup");
          }
        } catch (error) {
          console.log(error);
          throw new Error(error);
        }
      }
    } catch (error) {
      throw new Error(error.message);
    }
  };
};

export const autologin = (userId, token) => {
  return {
    type: AUTOLOGIN,
    userId: userId,
    token: token,
  };
};

export const logout = () => {
  AsyncStorage.setItem("userData", "");
  return { type: LOGOUT };
};

const saveDataToStorage = (token, userId) => {
  AsyncStorage.setItem(
    "userData",
    JSON.stringify({
      token: token,
      userId: userId,
    })
  );
};
