import { StyleSheet } from "react-native";
import Theam from "../../constants/Theme";

import { Dimensions } from "react-native";
import Theme from "../../constants/Theme";

const { width } = Dimensions.get("window");
const qrSize = width;

console.log(qrSize);
console.log(width);
const styles = StyleSheet.create({
  screen: {
    // flex: 1,
    width: Theme.deviceWidth,
    height: Theme.deviceHeight,
    // flexDirection: "column",
    // justifyContent: "flex-end",
    backgroundColor: "#000",

    // marginBottom: 0,
  },
  container: {
    // flex: 1,
    // width: Theam.deviceWidth,
    // height: Theam.deviceHeight,
    // alignItems: "center",
    margin: 0,
    padding: 0,
    // backgroundColor: "red",
    marginHorizontal: 0,
    marginLeft: 0,
    marginStart: 0,
    paddingHorizontal: 0,
    paddingLeft: 0,
    paddingStart: 0,
    height: "100%",
    padding: 0,
    // marginBottom: -50,
  },
  qr: {
    marginTop: "30%",
    // marginBottom: "20%",
    width: qrSize,
    height: qrSize,
    position: "absolute",
  },
  description: {
    fontSize: width * 0.09,
    marginTop: "10%",
    textAlign: "center",
    width: "70%",
    color: "#000",
  },
  cancel: {
    fontSize: width * 0.05,
    textAlign: "center",
    width: "70%",
    color: "white",
  },
  prevScansView: {
    position: "absolute",
    bottom: "10%",
    width: Theme.deviceWidth,
    flexDirection: "row",
    // marginTop: "90%",
    // marginBottom: 10,
    paddingHorizontal: 10,
  },
  prevScanRest: {
    width: Theme.deviceWidth * 0.25,
    overflow: "hidden",

    padding: 5,
    alignItems: "center",
    // borderWidth: 1,
    marginRight: 5,
    borderRadius: 5,
    // borderColor: "#fff",
  },
});

export default styles;
