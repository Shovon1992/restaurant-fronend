import React, { useState } from "react";
import {
  Text,
  View,
  KeyboardAvoidingView,
  Image,
  TouchableOpacity,
} from "react-native";
import styles from "../LoginScreen/loginStyle/";

const ForgotPasswordScreen = (props) => {
  const [loadingMsg, setLoadingMsg] = useState("Submit");
  const [isLoading, setIsLoading] = useState(false);
  return (
    <View style={styles.container}>
      <KeyboardAvoidingView behavior="height">
        <View style={styles.part1}>
          <Image
            source={require("../../../assets/images/menume-logo.png")}
            style={styles.logo}
          ></Image>
        </View>
        <View style={styles.part2}>
          {/* <View>
            <Text style={{ color: Theme.grayColor }}>
              Enter OTP sent on {userName}
            </Text>
          </View>
          <View style={styles.otpBoxesContainer}>
            <CodeInput
              ref={inputRef}
              // secureTextEntry
              className={"border-box"}
              activeColor={Theme.grayColor}
              inactiveColor="#bab5b1"
              space={Theme.deviceWidth / 21}
              keyboardType="numeric"
              autoFocus={true}
              codeLength={6}
              size={Theme.deviceWidth * 0.11}
              inputPosition="center"
              onFulfill={(code) => confirmSignup(code)}
            />
          </View> */}
          {/* <View>
            

            {error.password && (
              <View style={styles.errorContainer}>
                <Text style={styles.errorText}>{error.password}</Text>
              </View>
            )}
          </View> */}

          {/* {isLoading ? (
            <View style={styles.loading}>
              <ActivityIndicator size="large" color="black" />
              <Text style={{ textAlign: "center" }}>Validating</Text>
            </View>
          ) : (
            <View style={styles.buttonWrapper}>
              <View style={(styles.bottomLink, { alignSelf: "flex-end" })}>
                <TouchableOpacity onPress={() => {}}>
                  <Text style={styles.bottomLinkText}>Re-Send OTP</Text>
                </TouchableOpacity>
              </View>
              {error.apiError && (
                <View style={styles.errorContainer}>
                  <Text style={styles.errorText}>{error.apiError}</Text>
                </View>
              )}
              {error.apiError && (
                <View style={styles.errorContainer}>
                  <Text style={styles.errorText}>{error.apiError}</Text>
                </View>
              )}

              <View style={(styles.buttonMainContainer, styles.loginBtn)}>
                <CustomButton
                  onPress={confirmSignup}
                  title="Cofirm OTP"
                  style={styles.loginBtnStyle}
                />
              </View>
            </View>
          )} */}
        </View>
      </KeyboardAvoidingView>
    </View>
  );
};

ForgotPasswordScreen.navigationOptions = {
  headerTitle: "Forgot Password",
  headerTransparent: true,
};

export default ForgotPasswordScreen;
