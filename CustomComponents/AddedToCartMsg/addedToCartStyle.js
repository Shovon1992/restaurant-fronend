import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
    container : {
        marginTop : 10,
        flex: 1,
        alignSelf : 'center',
        alignItems : 'center',
        justifyContent: 'center',
        borderRadius : 35,
        backgroundColor : 'grey',
        padding : 25,
        shadowColor : 'black',
        shadowOpacity : 0.26,
        shadowOffset : {width : 0, height : 2},
        opacity : 1
    },
    message : {
      //  fontSize : 10,
        color : 'white'
    }
});

export default styles;