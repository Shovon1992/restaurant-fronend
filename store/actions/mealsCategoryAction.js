import Category from '../../model/categories';
import config from "../../constants/config";

export const FETCH_CATEGORY = 'FETCH_CATEGORY';

/**
 * get all food menue list 
 * @param {*} restaurantId is id of a particular restaurant 
 */
export const fetchCategory = (restaurantId) =>{
    return async dispatch =>{
        try{
            const response = await fetch ( config.foodCatagory+'/'+restaurantId,{
                method: 'GET',
                headers: config.headers
            });
            const resData = await response.json();
            var loadedCategories = [];
            for(let element of resData.data){
                    loadedCategories.push(new Category(
                        element.hash_id,
                        element.name,
                        element.thumbUrl,
                        element.description
                    ))
            }
            dispatch({  type : FETCH_CATEGORY, categories : loadedCategories })
        }catch(error){
            throw new Error(error);
        }
       
      
    }
}

