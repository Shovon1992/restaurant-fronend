import React, { useEffect } from "react";
import { View, AsyncStorage } from "react-native";
import { useDispatch } from "react-redux";

import * as AuthAction from "../../store/actions/authAction";
import Loader from "../../CustomComponents/Loader/loader";
const PersistantLogin = (props) => {
  const dispatch = useDispatch();

  useEffect(() => {
    const tryLogin = async () => {
      const userData = await AsyncStorage.getItem("userData");

      // console.log('userData persistant',userData);

      if (!userData) {
        props.navigation.navigate("Auth");
        return;
      }
      const transformedData = JSON.parse(userData);
      const { token, userId } = transformedData;

      if (!token || !userId) {
        props.navigation.navigate("Auth");
        return;
      }

      dispatch(AuthAction.autologin(userId, token));
      props.navigation.navigate("Drawer");
    };

    tryLogin();
  }, [dispatch]);

  return (
    <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
      <Loader></Loader>
    </View>
  );
};

export default PersistantLogin;
