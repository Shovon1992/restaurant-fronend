import React, { useEffect, useState, useCallback } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  FlatList,
  ImageBackground,
  StatusBar,
  Button,
  SafeAreaView,
} from "react-native";
import styles from "./MealCategoryStyle";
import { Ionicons } from "@expo/vector-icons";
/**
 * Import meal Action for the api call
 */
import * as mealsActions from "../../../store/actions/mealsCategoryAction";

/* get data from the reducers using redux */
import { useSelector, useDispatch } from "react-redux";
import Theme from "../../../constants/Theme";
import Loader from "../../../CustomComponents/Loader/loader";

import fetchCartForHeader from "../../../CustomComponents/CartFetchOnScreen/cartFetchFunction";
import CallWaiter from "../../../CustomComponents/CallWaiter/CallWaiter";

const MealsCategoryScreen = (props) => {
  const dispatch = useDispatch();
  const [isLoading, setIsLoading] = useState(false);
  const [isRefreshing, setIsRefreshing] = useState(false);
  const meals = useSelector((state) => state.meals.categories);
  const selectedTable = useSelector((state) => state.info.selectedTable);

  /* On Screen landing load check for Cart Items  */
  fetchCartForHeader(props);
  /* End On Screen landing load check for Cart Items  */

  /* fetch the categories with respect to restaurant ID */

  const loadMeals = useCallback(async () => {
    setIsLoading(true);
    setIsRefreshing(true);
    console.log('table ',selectedTable)
    try {
      await dispatch(mealsActions.fetchCategory(selectedTable.id));
    } catch (err) {
      setIsLoading(false);
    }
    setIsLoading(false);
    setIsRefreshing(false);
  }, [dispatch, setIsLoading]);

  /* for loading the food categories */
  useEffect(() => {
    loadMeals();
  }, [dispatch]);

  /* fetch category list and store in a variable */
  const categoryList = (itemData) => {
    return (
      <View>
        <TouchableOpacity
          style={styles.gridItems}
          onPress={() => {
            props.navigation.navigate({
              routeName: "Foods",
              params: {
                categoryId: itemData.item.id,
                catagoryImage: itemData.item.imageUrl,
                catagory: itemData.item.title,
              },
            });
          }}
        >
          {/* // <View styles={styles.itemImage}> */}
          <ImageBackground
            source={{ uri: itemData.item.imageUrl }}
            style={styles.itemImage}
          ></ImageBackground>
          {/* </View> */}
          <View style={styles.itemInfo}>
            <View>
              <Text style={styles.itemHeader}>{itemData.item.title}</Text>
            </View>
            <View>
              <Text style={styles.itemDetails} numberOfLines={2}>
                {itemData.item.description}
              </Text>
            </View>
          </View>
        </TouchableOpacity>
      </View>
    );
  };

  /* return the data as a flatlist */
  if (isLoading) {
    return <Loader></Loader>;
  } else {
    if (meals && meals[0]) {
      return (
        <View
          style={{
            position: "relative",
            height: Theme.deviceHeight,
            width: Theme.deviceWidth,
          }}
        >
          <View
            style={{ position: "absolute", bottom: 80, right: 5, zIndex: 9999 }}
          >
            <CallWaiter></CallWaiter>
          </View>

          <View>
            <StatusBar
              backgroundColor={Theme.background}
              barStyle="dark-content"
            />

            <FlatList
              onRefresh={loadMeals}
              refreshing={isRefreshing}
              //  keyExtractor={(item,index)=>item.id}
              data={meals}
              renderItem={categoryList}
              numColumns={1}
            />
          </View>
        </View>
      );
    } else {
      return (
        <View
          style={{ flex: 1, alignContent: "center", justifyContent: "center" }}
        >
          <StatusBar
            backgroundColor={Theme.background}
            barStyle="dark-content"
          />
          <View style={{ flex: 1, top: 150 }}>
            <Ionicons
              name="md-sad"
              size={100}
              style={{ alignSelf: "center", color: "gray" }}
            ></Ionicons>
            <Text style={{ textAlign: "center", color: "gray", fontSize: 25 }}>
              Sorry !! No Food menu Found
            </Text>
          </View>
        </View>
      );
    }
  }
};

MealsCategoryScreen.navigationOptions = {
  headerTitle: "Menu ",
};

export default MealsCategoryScreen;
