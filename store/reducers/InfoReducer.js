import { SELECTTABLE } from "../actions/InfoAction";
const initialState = {
  id: {},
  selectedTable: {},
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SELECTTABLE:
      return {
        ...state,
        selectedTable: action.data,
      };
  }

  return state;
};
