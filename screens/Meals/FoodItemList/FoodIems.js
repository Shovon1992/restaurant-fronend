import React, { useState, useCallback, useEffect } from "react";
import styles from "./FoodItemsStyle";
import {
  View,
  Text,
  ImageBackground,
  FlatList,
  StatusBar,
  Image,
  Button,
} from "react-native";
import {
  TouchableOpacity,
  TouchableHighlight,
} from "react-native-gesture-handler";
import Icon from "react-native-vector-icons/FontAwesome";
import { useSelector, useDispatch } from "react-redux";

import { Ionicons } from "@expo/vector-icons";
import Loader from "../../../CustomComponents/Loader/loader";
import * as foodItemActions from "../../../store/actions/foodItemAction";
import * as cartActions from "../../../store/actions/cartAction";

import Theme from "../../../constants/Theme";
import AddedToCartMsg from "../../../CustomComponents/AddedToCartMsg/addedToCartMsg";
import fetchCartForHeader from "../../../CustomComponents/CartFetchOnScreen/cartFetchFunction";

const FoodItems = (props) => {
  const dispatch = useDispatch();

  const [isLoading, setIsLoading] = useState(false);
  const [isRefreshing, setIsRefreshing] = useState(false);
  const [addingToCart, setAddingToCart] = useState(false);

  const categoryId = props.navigation.getParam("categoryId");
  const catagoryImage = props.navigation.getParam("catagoryImage");
  const catagory = props.navigation.getParam("catagory");

  /* On Screen landing load check for Cart Items  */
  fetchCartForHeader(props);
  /* End On Screen landing load check for Cart Items  */

  const loadFoodItems = useCallback(async () => {
    setIsLoading(true);
    setIsRefreshing(true);
    try {
      await dispatch(foodItemActions.fetchFoodItems(categoryId));
    } catch (err) {
      setIsLoading(false);
    }
    setIsLoading(false);
    setIsRefreshing(false);
  }, [dispatch, setIsLoading]);

  /* for loading the food categories */
  useEffect(() => {
    loadFoodItems();
  }, [dispatch]);

  const foodItems = useSelector((state) => state.foodItems.foodItems);

  const addItemToCart = (item) => {
    console.log('screen page ---',item)
    try {
      setAddingToCart(true);
      dispatch(cartActions.addToCart(item));
    } catch (error) {
      setAddingToCart(false);
    }
    setTimeout(() => {
      setAddingToCart(false);
    }, 2000);
  };

  const foodLists = (itemData) => {
    return (
      <View style={styles.gridItems}>
        {/* // <View styles={styles.itemImage}> */}
        <ImageBackground
          source={{
            uri: itemData.item.thumbUrl
              ? itemData.item.thumbUrl
              : "https://menume-images.s3.amazonaws.com/public/restaurant/welcomeScreen/WhatsApp%20Image%202020-07-31%20at%207.47.05%20PM.jpeg",
          }}
          style={styles.itemImage}
        ></ImageBackground>
        {/* </View> */}
        <View style={styles.itemInfo}>
          <View>
            <Text style={styles.itemHeader}>{itemData.item.name}</Text>
          </View>
          <View>
            <Text
              style={styles.itemDetails}
              numberOfLines={2}
              ellipsizeMode="tail"
            >
              {itemData.item.description}
            </Text>
          </View>
          <View>
            <Text style={styles.itemPrice}>
              <Text style={styles.currency}>{itemData.item.currency}</Text>
              RS {itemData.item.price} / {itemData.item.unit}
            </Text>
          </View>
        </View>

        <View style={styles.itemAction}>
          <View style={{ alignContent: "center" }}>
            { console.log('++++++++++++++++++++++++++++++++ item data ',itemData) }
            <TouchableOpacity
              style={styles.addBtn}
              onPress={addItemToCart.bind(this, itemData.item)}
            >
              <Text
                style={{
                  textAlign: "center",
                  color: "#71c90c",
                  padding: 2,
                }}
              >
                +Add
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  };

  /**
   * Main component to return
   */
  if (isLoading) {
    return <Loader></Loader>;
  } else {
    if (foodItems && foodItems[0]) {
      return (
        <View>
          <StatusBar
            backgroundColor={Theme.background}
            barStyle="dark-content"
          />

          <ImageBackground
            source={{ uri: catagoryImage }}
            style={styles.bgImage}
          >
            <View style={styles.titleContainer}>
              <Text style={styles.title}>{catagory}</Text>
            </View>
          </ImageBackground>
          <FlatList
            // keyExtractor={(item,index)=>item.id}
            onRefresh={loadFoodItems}
            refreshing={isRefreshing}
            data={foodItems}
            renderItem={foodLists}
            numColumns={1}
          />

          {addingToCart === true && <AddedToCartMsg msg="Item Added to Cart" />}
        </View>
      );
    } else {
      return (
        <View>
          <ImageBackground
            source={{ uri: catagoryImage }}
            style={styles.bgImage}
          >
            <View style={styles.titleContainer}>
              <Text style={styles.title}>{catagory}</Text>
            </View>
          </ImageBackground>
          <View style={{ flex: 1, top: 150 }}>
            <Ionicons
              name="md-sad"
              size={100}
              style={{ alignSelf: "center", color: "gray" }}
            ></Ionicons>
            <Text style={{ textAlign: "center", color: "gray", fontSize: 25 }}>
              Sorry !! No {catagory} item Found
            </Text>
          </View>
        </View>
      );
    }
  }
};
FoodItems.navigationOptions = {
  headerTitle: "Food Items",
};

export default FoodItems;
