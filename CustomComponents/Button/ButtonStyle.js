import { StyleSheet } from "react-native";
import Theam from "../../constants/Theme";
const styles = StyleSheet.create({
  buttonContainer: {
    elevation: 2,
    backgroundColor: Theam.greenColor,
    //borderRadius: 4,
    //  paddingVertical: 10,
    //  paddingHorizontal: 12,
    width: "100%",
    height: 50,
    justifyContent: "center",
  },
  buttonText: {
    fontSize: 17,
    //  top: 5,
    color: "#fff",
    fontWeight: "bold",
    alignSelf: "center",
    textTransform: "uppercase",
  },
  //  iconTextContainer : {
  //     flexDirection : 'row',
  //  }
});

export default styles;
