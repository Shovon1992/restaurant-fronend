export default class Cart {
    constructor(item,quantity,price,image,sum){
        this.item = item,
        this.quantity = quantity,
        this.price = price,
        this.image = image,
        this.sum = sum
    }
}