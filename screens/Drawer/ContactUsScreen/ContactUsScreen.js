import React, { useState } from "react";
import {
  View,
  Text,
  Image,
  ScrollView,
  TextInput,
  TouchableOpacity,
} from "react-native";

import styles from "./ContactUsStyles";
import Icon from "react-native-vector-icons/FontAwesome";

import { HeaderButtons, Item } from "react-navigation-header-buttons";
// import customHeaderButton from "../../../CustomComponents/HeaderButton/HeaderButton";
import customHeaderButton from "../../../CustomComponents/HeaderButton/HeaderButton";
import Theme from "../../../constants/Theme";
import CustomButton from "../../../CustomComponents/Button/Button";

const ContactUsScreen = (props) => {
  const [parameters, setParameters] = useState({
    name: "",
    subject: "",
    email: "",
    message: "",
  });
  const inputHandler = (key, value) => {
    setParameters((prevState) => ({
      ...prevState,
      [key]: value,
    }));
  };
  return (
    <View style={styles.container}>
      <ScrollView
        style={{
          width: Theme.deviceWidth,
          paddingHorizontal: 10,
        }}
      >
        <View style={styles.imageView}>
          <Image
            source={require("../../../assets/images/menume-logo.png")}
            style={styles.logo}
          ></Image>
        </View>
        <View>
          <View style={{ flexDirection: "row" }}>
            <View style={styles.IconView}>
              <Icon
                name="map-marker"
                size={Theme.iconSize}
                style={styles.headingLeftIcon}
              />
            </View>
            <View style={styles.infoView}>
              <Text style={styles.heading}>Visit our location</Text>
              <Text>472 East 180 South Kolkata, West Bengal, 713345</Text>
            </View>
          </View>
        </View>
        <View>
          <View style={{ flexDirection: "row" }}>
            <View style={styles.IconView}>
              <Icon
                name="mobile"
                size={Theme.iconSize * 0.8}
                style={styles.headingLeftIcon}
              />
            </View>
            <View style={styles.infoView}>
              <Text style={styles.heading}>Give us a call</Text>
              <Text>(mob) : (+91) 1800800800</Text>
            </View>
          </View>
        </View>

        <View>
          <View style={{ flexDirection: "row" }}>
            <View style={styles.IconView}>
              <Icon
                name="envelope-o"
                size={Theme.iconSize * 0.8}
                style={styles.headingLeftIcon}
              />
            </View>
            <View style={styles.formView}>
              <View>
                <Text style={styles.heading}>Have a question for MenuMe ?</Text>
                <Text>Email us Below to ask.</Text>
              </View>
              <View style={styles.inputView}>
                <TextInput
                  style={styles.textInput}
                  placeholder="Name"
                  borderWidth={1}
                  borderColor={Theme.grayColor}
                  borderRadius={3}
                  placeholderTextColor={Theme.grayColor}
                  onChangeText={(value) => inputHandler("name", value)}
                  autoCapitalize="none"
                  theme={{
                    colors: { primary: Theme.accentColor, color: "gray" },
                  }}
                />
              </View>
              <View style={styles.inputView}>
                <TextInput
                  style={styles.textInput}
                  placeholder="Subject"
                  borderWidth={1}
                  borderColor={Theme.grayColor}
                  borderRadius={3}
                  placeholderTextColor={Theme.grayColor}
                  onChangeText={(value) => inputHandler("subject", value)}
                  autoCapitalize="none"
                />
              </View>
              <View style={styles.inputView}>
                <TextInput
                  style={styles.textInput}
                  placeholder="Email"
                  borderWidth={1}
                  borderColor={Theme.grayColor}
                  borderRadius={3}
                  placeholderTextColor={Theme.grayColor}
                  onChangeText={(value) => inputHandler("email", value)}
                  autoCapitalize="none"
                />
              </View>
              <View style={styles.inputView}>
                <TextInput
                  style={styles.textInput}
                  placeholder="Message"
                  borderWidth={1}
                  borderColor={Theme.grayColor}
                  borderRadius={3}
                  placeholderTextColor={Theme.grayColor}
                  onChangeText={(value) => inputHandler("message", value)}
                  numberOfLines={5}
                  autoCapitalize="none"
                />
              </View>
              <View style={styles.buttonView}>
                <CustomButton
                  onPress={() => {}}
                  title="Ask Question"
                ></CustomButton>
              </View>
            </View>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

ContactUsScreen.navigationOptions = (navData) => {
  return {
    headerTitle: "Contact Us",
    headerLeft: () => (
      <HeaderButtons HeaderButtonComponent={customHeaderButton}>
        <Item
          title="Menu"
          iconName="ios-menu"
          onPress={() => {
            navData.navigation.toggleDrawer();
          }}
        />
      </HeaderButtons>
    ),
  };
};

export default ContactUsScreen;
