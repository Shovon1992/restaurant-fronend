import React, { useEffect, useState } from "react";
import {
  View,
  Text,
  TextInput,
  ActivityIndicator,
  TouchableOpacity,
  ImageBackground,
  Platform,
  Image,
  ScrollView,
  AsyncStorage,
} from "react-native";
import styles from "./loginStyle";
import { validateAll } from "indicative/validator";
import { useDispatch } from "react-redux";
import * as authActions from "../../../store/actions/authAction";
import Theam from "../../../constants/Theme";

import Message from "../../../constants/Message";
import Card from "../../../CustomComponents/Card/card";
import CustomButton from "../../../CustomComponents/Button/Button";
import Icon from "react-native-vector-icons/FontAwesome";
import Loader from "../../../CustomComponents/Loader/loader";

import * as Device from "expo-device";

const LoginScreen = (props) => {
  const dispatch = useDispatch();
  const [userName, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [deviceInfo, setDeviceInfo] = useState();

  const [showPassword, setShowPassword] = useState(true);
  const [showPasswordIcon, setShowPasswordIcon] = useState("eye-slash");

  const toggleShowPassword = () => {
    let show = showPassword ? false : true;
    let icon = showPassword ? "eye" : "eye-slash";

    setShowPassword(show);
    setShowPasswordIcon(icon);
  };

  let state = {
    userName: userName,
    password: password,
  };
  /**
   * For show or hide password
   */
  /*const changeShow = () => {
    state.secureTextEntry = state.secureTextEntry ? false : true;
    state.secureIcon = state.secureTextEntry ? "md-eye" : "md-eye-off";
    // setState({ 'secureTextEntry' : tempPass });
    // setState({ 'secureIcon' : tempIcon });
    alert(state.secureTextEntry, state.secureIcon);
  };*/
  const Login = async () => {
    const rules = {
      userName: "required|email",
      password: "required|min:6",
    };

    const messages = {
      required: (field) => `${field} is required`,
      "userName.email": Message.validEmail,
      "password.min": Message.passwordMin,
    };

    const deviceToken = await AsyncStorage.getItem("DeviceToken");

    try {
      // console.log(
      //   "Device Info :",
      //   JSON.stringify(deviceInfo, null, ""),
      //   deviceToken
      // );

      setIsLoading(true);
      setError({});
      const validateForm = await validateAll(state, rules, messages);

      if (validateForm) {
        await dispatch(
          authActions.login(
            state.userName,
            state.password,
            deviceInfo.uuid,
            deviceInfo.deviceType,
            deviceInfo.deviceName,
            deviceToken
          )
        );
        setIsLoading(false);
        props.navigation.navigate("home");
      }
    } catch (err) {
      const formattedErrors = {};
      const checkAPIResponse = Array.isArray(err);

      // err = JSON.parse(err);
      setIsLoading(false);
      // console.log(checkAPIResponse, err);
      //await dispatch(authActions.login(state.userName, state.password))   ;
      if (err == "Error: User is not confirmed.") {
        // console.log("jagfjhagfhjgdjhgfjh");
        props.navigation.navigate("ConfirmSignup", {
          userName: state.userName,
        });
      } else {
        if (checkAPIResponse !== true) {
          formattedErrors["apiError"] = "" + err + "";
        } else {
          err.forEach(
            (error) => (formattedErrors[error.field] = error.message)
          );
        }

        setError(formattedErrors);
        setIsLoading(false);
      }
    }
  };

  useEffect(() => {
    /* get device info */
    getDeviceInfo = async () => {
      let deviceJSON = {};
      try {
        // deviceJSON.deviceModel = await Device.modelName;
        // deviceJSON.deviceBrand = await Device.manufacturer;
        deviceJSON.uuid = await Device.osBuildId;
        deviceJSON.deviceName = await Device.modelName;
        deviceJSON.deviceType = "Android Mobile";
      } catch (error) {
        // console.log(error);
      }
      // console.log(deviceJSON);
      setDeviceInfo(deviceJSON);
    };
    getDeviceInfo();
  }, []);

  return (
    <View style={styles.container}>
      {isLoading ? <Loader></Loader> : <View></View>}
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.part1}>
          <Image
            source={require("../../../assets/images/menume-logo.png")}
            style={styles.logo}
          ></Image>
        </View>
        <View style={styles.part2}>
          <View style={styles.inputContainer}>
            <Icon
              name="user"
              size={Theam.iconSize}
              style={styles.InputRightIcon}
            />
            <TextInput
              style={styles.textInput}
              placeholder="User Name"
              onChangeText={(userName) => setUsername(userName)}
              keyboardType="email-address"
              autoCapitalize="none"
            />
          </View>
          {error.userName && (
            <View style={styles.errorContainer}>
              <Text style={styles.errorText}>{error.userName}</Text>
            </View>
          )}
          <View style={styles.inputContainer}>
            <Icon
              name="lock"
              size={Theam.iconSize}
              style={styles.InputRightIcon}
            />
            <TextInput
              style={styles.textInput}
              placeholder="Password"
              keyboardType="default"
              secureTextEntry={showPassword}
              onChangeText={(password) => setPassword(password)}
              onPress={() => {
                error.username = "";
              }}
            />

            <TouchableOpacity
              onPress={() => toggleShowPassword()}
              style={{ justifyContent: "center" }}
            >
              <Icon
                name={showPasswordIcon}
                size={Theam.iconSize}
                style={styles.InputRightIcon}
              />
            </TouchableOpacity>
          </View>
          {error.password && (
            <View style={styles.errorContainer}>
              <Text style={styles.errorText}>{error.password}</Text>
            </View>
          )}

          {/* {isLoading ? (<Loader></Loader> ) : ( */}
          <View style={styles.buttonWrapper}>
            {/* <View style={(styles.bottomLink, { alignSelf: "flex-end" })}>
              <TouchableOpacity
                onPress={() => {
                  // props.navigation.navigate("ForgotPassword");
                }}
              >
                <Text style={styles.bottomLinkText}>Forgot Password?</Text>
              </TouchableOpacity>
            </View> */}
            {error.apiError && (
              <View style={styles.errorContainer}>
                <Text style={styles.errorText}>{error.apiError}</Text>
              </View>
            )}

            <View style={(styles.buttonMainContainer, styles.loginBtn)}>
              <CustomButton
                onPress={Login}
                title="Login "
                style={styles.loginBtnStyle}
              />
            </View>

            <View style={styles.textToOption}>
              <View style={styles.lineStyle}></View>
              <Text style={styles.loginButtonBelowText}> Or Login With</Text>
              <View style={styles.lineStyle}></View>
            </View>

            <View style={styles.OtherLoginButtonContainer}>
              <View style={styles.buttonMainContainer}>
                <CustomButton
                  onPress={() => {}}
                  title="Facebook "
                  style={styles.FacebookButtonStyle}
                  IconButtonStyle={styles.IconButtonStyle}
                  buttonText={styles.SocialbuttonText}
                >
                  <Icon
                    name="facebook"
                    size={Theam.iconSize}
                    style={{ ...styles.InputRightIcon, color: "white" }}
                  />
                </CustomButton>
              </View>
              <View style={styles.buttonMainContainer}>
                <CustomButton
                  onPress={() => {}}
                  title="Google "
                  style={styles.GoogleButtonStyle}
                  IconButtonStyle={styles.IconButtonStyle}
                  buttonText={styles.SocialbuttonText}
                  disabled={true}
                >
                  <Icon
                    name="google"
                    size={Theam.iconSize}
                    style={{ ...styles.InputRightIcon, color: "white" }}
                  />
                </CustomButton>
              </View>
            </View>
          </View>

          {/* )}   */}
        </View>
      </ScrollView>
    </View>
  );
};

LoginScreen.navigationOptions = {
  headerTitle: "",
  headerTransparent: true,
};

export default LoginScreen;
