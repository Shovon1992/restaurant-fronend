import Category from '../model/categories';
import Welcome from '../model/welcome';
import Cart from '../model/cart';
import QRcode from '../model/QRcode';

export const CATEGORIES = [
    new Category('c1', 'Indian', 'https://images.pexels.com/photos/1640777/pexels-photo-1640777.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940'),
    new Category('c2', 'Italian', 'https://images.pexels.com/photos/4194378/pexels-photo-4194378.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500'),
    new Category('c3', 'Mexican', 'https://images.pexels.com/photos/4194382/pexels-photo-4194382.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500'),
    new Category('c4', 'Continental', 'https://images.pexels.com/photos/4193848/pexels-photo-4193848.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500')
  ];

export const WELCOME = [
    new Welcome (   
      'https://images.unsplash.com/photo-1517248135467-4c7edcad34c4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80',
       'Welome to Restaurant',
       'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,'
    ),
    new Welcome (
       'https://static.toiimg.com/photo/53520366.cms',
        'Enjoy the meal',
        'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,'
    ),
    new Welcome  (
       'https://www.posist.com/restaurant-times/wp-content/uploads/2016/03/bar-601318_1920-800x492.jpg',
        'Lets start',
        'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,'
    )
];


export const CART = [
  new Cart('1', 'Omlet', 3, 3.56,'https://www.posist.com/restaurant-times/wp-content/uploads/2016/03/bar-601318_1920-800x492.jpg'),
  new Cart('2', 'Bread Butter', 4 , 7.96, 'https://www.posist.com/restaurant-times/wp-content/uploads/2016/03/bar-601318_1920-800x492.jpg'),
  // new Cart('3', 'Chiken Fry', 1, 17.69, 'https://www.posist.com/restaurant-times/wp-content/uploads/2016/03/bar-601318_1920-800x492.jpg'),
  // new Cart('4', 'Test 1', 1, 3.56,'https://www.posist.com/restaurant-times/wp-content/uploads/2016/03/bar-601318_1920-800x492.jpg'),
  // new Cart('5', 'Test 2', 2, 7.96, 'https://www.posist.com/restaurant-times/wp-content/uploads/2016/03/bar-601318_1920-800x492.jpg'),
  // new Cart('6', 'Test 3', 1, 17.69, 'https://www.posist.com/restaurant-times/wp-content/uploads/2016/03/bar-601318_1920-800x492.jpg'),
  // new Cart('7', 'Test 4', 1, 17.69, 'https://www.posist.com/restaurant-times/wp-content/uploads/2016/03/bar-601318_1920-800x492.jpg'),
];

export const QRCODE = [
  new QRcode('1','Taj Hotel','blah blah blah blah blah blah blah blah','https://static.toiimg.com/photo/53520366.cms'),
  new QRcode('2','Hyat Regency','blah blah blah blah blah blah blah blah','https://www.posist.com/restaurant-times/wp-content/uploads/2016/03/bar-601318_1920-800x492.jpg')
]