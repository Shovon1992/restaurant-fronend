import { NOTIFICATION_ADD, NOTIFICATION_GET } from "../actions/notification";

const initialState = {
  notification: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case NOTIFICATION_GET:
      return {
        ...state,
        notification: action.data,
      };
    case NOTIFICATION_ADD:
    default:
      return state;
  }
};
