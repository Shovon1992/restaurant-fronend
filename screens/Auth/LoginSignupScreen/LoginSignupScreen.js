import React from "react";
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  TouchableWithoutFeedback,
} from "react-native";
import styles from "./LoginSigupStyle";
import CustomButton from "../../../CustomComponents/Button/Button";

const LoginSignupScreen = (props) => {
  return (
    <View style={styles.screen}>
      <View style={styles.headerLogo}>
        <Image
          source={require("../../../assets/images/menume-logo.png")}
          style={styles.logo}
        ></Image>
      </View>
      <View style={styles.textContainer}>
        <Text style={styles.text}>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam, quis nostrud exercitation ullamco laboris nisi ut
          aliquip ex ea commodo consequat.
        </Text>
      </View>
      <View style={styles.buttonWrapper}>
        <TouchableWithoutFeedback
          onPress={() => {
            props.navigation.navigate("Login");
          }}
        >
          <View style={styles.buttonMainContainer}>
            <CustomButton
              onPress={() => {
                props.navigation.navigate("Login");
                // props.navigation.navigate("ConfirmSignup");
              }}
              title="Login "
              style={styles.button}
            />
          </View>
        </TouchableWithoutFeedback>
        <TouchableWithoutFeedback
          onPress={() => {
            props.navigation.navigate("Signup");
          }}
        >
          <View style={styles.buttonMainContainer}>
            <CustomButton
              onPress={() => {
                props.navigation.navigate("Signup");
              }}
              title="Register "
              buttonText={styles.registerButtonText}
              style={styles.registerButton}
            />
          </View>
        </TouchableWithoutFeedback>
      </View>
    </View>
  );
};
LoginSignupScreen.navigationOptions = {
  headerTitle: "",
  headerTransparent: true,
};

export default LoginSignupScreen;
