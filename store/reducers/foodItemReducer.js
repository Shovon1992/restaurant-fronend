
import {FETCH_FOOD_ITEMS} from '../actions/foodItemAction';

const initialState = {
    foodItems : null,
}

export default (state = initialState, action) => {
    switch(action.type){
        case FETCH_FOOD_ITEMS :
            return {
                 foodItems : action.fooditems
                }
        default : 
            return state;
    }
}
