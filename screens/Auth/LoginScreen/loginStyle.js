import { StyleSheet } from "react-native";
import Theam from "../../../constants/Theme";
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: Theam.background,
  },
  part1: {
    flexDirection: "row",
    alignSelf: "center",
    height: Theam.deviceHeight * 0.3,
    marginBottom: 10,
    //flex: 1.5
  },
  part2: {
    alignContent: "center",
    width: Theam.deviceWidth * 0.9,
    height: Theam.deviceHeight * 0.7,
    //flex: 3
  },
  part3: {
    alignContent: "center",
    height: Theam.deviceHeight * 0.3,
    //flex: 1.5
  },
  logo: {
    borderRadius: 100,
    width: Theam.deviceHeight * 0.2,
    height: Theam.deviceHeight * 0.2,
    alignSelf: "flex-start",
    top: Theam.deviceHeight * 0.09,
  },
  inputContainer: {
    flexDirection: "row",
    borderBottomColor: "#696560",
    borderBottomWidth: 1,
    marginTop: 15,
    marginBottom: 5,
    width: Theam.deviceWidth * 0.9,
    // justifyContent: "flex-end",
  },
  textInput: {
    // justifyContent: "center",
    color: "black",
    height: 50,
    flex: 5,
    fontSize: Theam.textSize,
    left: Theam.deviceWidth * 0.03,
  },
  InputRightIcon: {
    paddingHorizontal: 2,
    alignSelf: "center",
    color: "#696560",
  },
  buttonWrapper: {
    marginTop: 20,
  },
  buttonMainContainer: {
    flex: 3,
    margin: 10,
    //backgroundColor: 'red'
    // paddingBottom : 5,
    // paddingTop : 5,
    //marginTop: 20,
    // top : 30,
    // width : '100%',
    // flex : 2
  },
  bottomLinkText: {
    marginTop: 15,
    color: Theam.greenColor,
    fontSize: Theam.fontSize,
  },
  errorContainer: {
    justifyContent: "center",
    // backgroundColor: "red",
    alignItems: "flex-end",
    // height: 50,
    width: "100%",
    // marginBottom: 10,
  },
  errorText: {
    color: "red",
    textTransform: "capitalize",
    // fontStyle: "italic",
    // lineHeight: 50,
  },
  textToOption: {
    flexDirection: "row",
    alignItems: "center",
    top: Theam.deviceHeight * 0.06,
  },
  lineStyle: {
    flex: 1,
    height: 1,
    backgroundColor: "black",
  },
  loginButtonBelowText: {
    fontSize: Theam.textSize,
    paddingTop: Theam.deviceHeight * 0.001,
    paddingHorizontal: 2,
    alignSelf: "center",
    color: "#A2A2A2",
  },
  OtherLoginButtonContainer: {
    flexDirection: "row",
    top: Theam.deviceHeight * 0.1,
  },
  IconButtonStyle: {
    flexDirection: "row",
  },
  ButtonStyle: {
    backgroundColor: "#4d94ff",
  },
  GoogleButtonStyle: {
    backgroundColor: "#db4a39",
    height: Theam.deviceHeight * 0.07,
    borderRadius: 5,
    paddingVertical: Theam.deviceHeight * 0.01,
    paddingHorizontal: 10,
  },
  FacebookButtonStyle: {
    backgroundColor: "#4267B2",
    height: Theam.deviceHeight * 0.07,
    borderRadius: 5,
    paddingVertical: Theam.deviceHeight * 0.01,
    paddingHorizontal: 10,
  },
  SocialbuttonText: {
    fontSize: Theam.bottonText,
    marginLeft: Theam.deviceWidth * 0.02,
    // marginBottom : Theam.deviceHeight*.02
  },
  buttonText: {
    //lineHeight : 20
  },
  loginBtn: {
    borderRadius: 10,
    marginTop: Theam.deviceHeight * 0.025,
    marginBottom: Theam.deviceHeight * 0.03,
  },
  loginBtnStyle: {
    borderRadius: 10,
    paddingVertical: Theam.deviceHeight * 0.01,
    paddingHorizontal: Theam.deviceWidth * 0.01,
    height: Theam.deviceHeight * 0.07,
  },
  otpBoxesContainer: {
    width: Theam.deviceWidth * 0.9,
    // backgroundColor: "red",
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "flex-start",
  },
});

export default styles;
