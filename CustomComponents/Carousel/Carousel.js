import React, { useRef } from "react";
import {
  SafeAreaView,
  ScrollView,
  Text,
  StyleSheet,
  View,
  ImageBackground,
  Animated,
  useWindowDimensions,
} from "react-native";
import Slide from "./Slide";
import styles from "./CarouselStyle";
export const Carousel = (props) => {
  let { items, style } = props;
  const scrollX = useRef(new Animated.Value(0)).current;
  //items = (items) ? items : welcomeDummyData;
  // console.log("carousole page ", items.header);
  const { width: windowWidth } = useWindowDimensions();
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.scrollContainer}>
        <ScrollView
          style={styles.scrollViewStyle}
          horizontal
          disableIntervalMomentum={true}
          showsHorizontalScrollIndicator={false}
          snapToInterval={windowWidth}
          onScroll={Animated.event(
            [
              {
                nativeEvent: {
                  contentOffset: {
                    x: scrollX,
                  },
                },
              },
            ],
            { useNativeDriver: false }
          )}
          scrollEventThrottle={1}
        >
          {items.map((item, itemIndex) => {
            return (
              <Slide
                key={itemIndex}
                details={item}
                navigation={props.navigation}
                restaurantId={props.restaurantId}
                navigateTo={props.navigateTo}
              />
            );
          })}
        </ScrollView>
        <View style={styles.indicatorContainer}>
          {items.map((item, itemIndex) => {
            const width = scrollX.interpolate({
              inputRange: [
                windowWidth * (itemIndex - 1),
                windowWidth * itemIndex,
                windowWidth * (itemIndex + 1),
              ],
              outputRange: [8, 16, 8],
              extrapolate: "clamp",
              useNativeDriver: true,
            });
            return (
              <Animated.View
                key={itemIndex}
                style={[styles.normalDot, { width, useNativeDriver: true }]}
                useNativeDriver={true}
              />
            );
          })}
        </View>
      </View>
    </SafeAreaView>
  );
};

export default Carousel;
