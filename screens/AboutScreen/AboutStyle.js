import { StyleSheet } from "react-native";
import Theme from "../../constants/Theme";

const styles = StyleSheet.create({
  container: {
    width: Theme.deviceWidth,
    // backgroundColor: "red",
    alignSelf: "center",
    // flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  imageView: {
    flexDirection: "row",
    alignSelf: "center",
    height: Theme.deviceHeight * 0.3,

    // marginBottom: 10,
  },
  logo: {
    borderRadius: 100,
    width: Theme.deviceHeight * 0.2,
    height: Theme.deviceHeight * 0.2,
    alignSelf: "flex-start",
    top: Theme.deviceHeight * 0.05,
  },
  infoView: {
    // borderBottomWidth: 1.5,
    // borderBottomColor: Theme.accentColor,
    paddingBottom: 20,
    marginBottom: 20,
  },
  heading: {
    fontSize: Theme.headerTextSize,
    fontWeight: "bold",
    lineHeight: 40,
  },
});

export default styles;
