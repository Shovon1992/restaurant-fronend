import { StyleSheet, Dimensions } from 'react-native';
var deviceHeight = Dimensions.get('window').height;
var deviceWidth = Dimensions.get('window').width;

import Theme from '../../constants/Theme';
const styles = StyleSheet.create({
    container : {
        flex : 1,
        alignItems : 'center',
        justifyContent : 'center',
        backgroundColor : Theme.background
    },
    slide: {
        //paddingHorizontal: 20,
        //paddingBottom: 10,
        //paddingTop: 30,
        height: deviceHeight,
        flexBasis: '100%',
        flex: 1,
        maxWidth: '100%',
        display: 'flex',
        flexDirection: 'row', 
        //flexWrap: 'wrap',
        alignItems: 'center',
        alignContent: 'center',
        justifyContent: 'center',
        //height: 200,
      },
      slideText: {
        width: '100%',
        textAlign: 'left',
        fontSize: 20,
      },
      bullets: {
        //position: 'absolute',
        // top: 0,
        // right: 0,
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'row',
        paddingHorizontal: 10,
        paddingTop: 5,
        paddingBottom: 20

      },
      bullet: {
        paddingHorizontal: 5,
        fontSize: 20,
      },
      card :{
          //height: deviceHeight-10,
         // width: '100%',
          //flex: 1,
          //flexDirection: 'row',
          //top: 10,
          //alignItems: 'stretch',
          height: deviceHeight,
          width: deviceWidth,
          backgroundColor: Theme.background
      },
      cardImage :{
          //height: 700 *.7
          //flex: .6,
          top: 0,
          resizeMode: 'contain',
          height: Theme.deviceHeight/2.5,
          width: deviceWidth,
      },
      cardTitle :{

          color: 'green',
          textAlign: 'center',
          fontSize: 30,
          marginBottom: 20,
          fontWeight: 'bold',
         
      },
      cardContent :{
        color: 'gray',
        fontSize: 20,
        textAlign: 'center',
         paddingBottom: 15
    },
    button :{
        width : deviceWidth*.4,
        alignSelf: 'center',
        position: 'absolute',
        bottom: deviceHeight*.051,
    },
    cardDescription:  {
        paddingTop : 50,
        //backgroundColor: 'red'
    },
    normalDot: {
        height: 8,
        width: 8,
        borderRadius: 4,
        backgroundColor: "#71c90c",
        marginHorizontal: 4
      },
      indicatorContainer: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
        bottom: 40
      }
});

export default styles;