import React, {useCallback, useEffect,useState} from 'react';
import {View, Text,FlatList,ImageBackground,ScrollView} from 'react-native';

import styles from './ThankyouStyle';
import { Ionicons } from '@expo/vector-icons'; 
import CustomButton from '../../CustomComponents/Button/Button';
import { useSelector, useDispatch } from 'react-redux';
import * as CartActions from '../../store/actions/cartAction';
import * as orderActions from '../../store/actions/orderAction';
//import { AnimatedCircularProgress } from 'react-native-circular-progress';

import CircularProgress from '../../CustomComponents/CircularProgress/circularProgress';

const ThankyouScreen = props => {
    const dispatch = useDispatch();
    const [isLoading, setIsLoading] = useState(false);
    const [isRefreshing, setIsRefreshing] = useState(false);
     /* fetch orders from state */
    const orderId = useSelector(state=>state.orders.orderId);
    const orders = useSelector(state=>state.orders.orders);
    const totalAmount = useSelector(state=>state.orders.totalAmount);
   // console.log('orders screen',orders);
    const fetchOrder = useCallback( async () => {
        setIsLoading(true);
        setIsRefreshing(true);
        try{
           await dispatch(orderActions.fetchOrderDetails(orderId));
        }catch (err) {
           console.log(err);
        }
        setIsRefreshing(false);
        setIsLoading(false);
    },[dispatch,setIsLoading,setIsRefreshing])
  
      /* Resist Rerender for loading products */
    useEffect(()=>{
        fetchOrder()
              if(orders.length !== 0){
                    dispatch(CartActions.emptyCart());
                }
    }, [dispatch,fetchOrder]);


   
   
    


    const orderedItemList = itemData => {
       // console.log('itemData',itemData);
        return (
            <View style={{flexDirection:'row', justifyContent : 'space-between', margin: 5}}>
               <ImageBackground source={{ uri: itemData.item.image }} style={styles.image}></ImageBackground>
                   <Text style={{marginTop: 25}}>{itemData.item.item}</Text>
                   <Text style={{marginTop: 25}} >{itemData.item.quantity}</Text>
                   <Text style={{marginTop: 25}} >Rs.{(itemData.item.price * itemData.item.quantity).toFixed(2)}</Text>
            </View>
        )
       
       
    }
 
    return (
        <View style={styles.screen}>
            <View style={styles.section1}>
                 <Ionicons name="md-checkmark-circle-outline" size={60} color="green" />
                 <View style={styles.containerTopTextWrapper}>
                    <Text style={styles.boldHeadText}>Thank You  </Text>
                    <Text style={styles.normalText}>Your Order is confirmed    </Text>
                 </View>
                 
            </View>
            <ScrollView>
            <View style={styles.section2}>
                        <View style={styles.sectionWrapper}>
                            <Text style={styles.textLeft}>Your OrdeID  </Text>
                            {/* <Text style={styles.textRight}>#7b89c5e737714436ddcb24f308784282b7b1ab5b {getLatestOrder[0].id}</Text>
                             */}
                            <Text style={styles.textRight} >#{orders[0]['id']}</Text>
                        </View>
            </View>
            
            <View style={styles.section4}>
                        <View style={styles.sectionWrapper4}>
                            <Text style={styles.textLeft}>Order Details  </Text>
                            {/* <View style={{
                                    flexDirection : 'row',
                                    justifyContent : 'space-between', 
                                    paddingHorizontal : 70,
                                    left : 80
                            }}>
                                <Text>Item</Text>
                                <Text>Price</Text>
                                <Text>Quantity</Text>
                            </View> */}
                            <FlatList
                                   data={orders}
                                   renderItem={orderedItemList}
                                   numColumns={1}
                            />
                        </View>
            </View>
            <View style={styles.section3}>
                        <View style={styles.sectionWrapper}>
                            <Text style={styles.textLeft}>Your Total Amount  </Text>
                            <Text style={{...styles.textRight, textAlign : 'right'}}>Rs. {totalAmount.toFixed(2)}</Text>
                        </View>
            </View>
            <View style={styles.section5}>
                        <View style={styles.sectionWrapper}>
                            <Text style={styles.textLeft,{top:20,fontSize : 16,
        fontWeight : 'bold',}}>Waiting Time  </Text>
                            
                            <CircularProgress></CircularProgress>

                            
                            {/* ß<Text style={styles.textRight}>20mins</Text> */}
                        </View>
            </View>
            <View style={styles.section6}>
                    <View style={styles.buttonMainContainer}>
                        <CustomButton 
                                onPress={()=>{}} 
                                title='Track Order  ' 
                                style={styles.button}
                        />
                    </View>
                    <View style={styles.buttonMainContainer}>
                            <CustomButton 
                                    onPress={()=>{props.navigation.navigate('Category')}} 
                                    title='Back to menu ' 
                                    buttonText={styles.backToMenuText} 
                                    style={styles.button, styles.backToMenuButton}
                            />
                    </View>
            </View>
            </ScrollView>
        </View>
    )
}


ThankyouScreen.navigationOptions = {
    headerTitle: '',
    headerTransparent: true
}

export default ThankyouScreen;