import { StyleSheet, Dimensions } from "react-native";
import Theme from "../../../constants/Theme";
var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;
const styles = StyleSheet.create({
  gridItems: {
    flex: 1,
    flexDirection: "row",
    margin: 5,
    height: deviceHeight * 0.15,
    borderRadius: 1,
    elevation: 2,
    shadowColor: "black",
    shadowOpacity: 0.12,
    shadowOffset: { width: 0, height: 1 },
    width: deviceWidth * 0.95,
    alignSelf: "center",
  },
  itemImage: {
    resizeMode: "cover",
    height: deviceHeight * 0.144,
    width: deviceHeight * 0.144,
    alignSelf: "center",
    top: 1,
    left: 1,
  },
  bgImage: {
    resizeMode: "contain",
    height: deviceHeight * 0.25,
    width: deviceWidth,
  },
  titleContainer: {
    backgroundColor: "rgba(0,0,0,0.5)",
    paddingVertical: 5,
    paddingHorizontal: 12,
    position: "absolute",
    bottom: 0,
    left: 0,
    right: 0,
  },
  title: {
    fontSize: 20,
    textAlign: "center",
    color: "white",
  },
  cartBtn: {
    position: "absolute",
    width: 60,
    height: 60,
    alignItems: "center",
    justifyContent: "center",
    right: 30,
    bottom: 30,
    backgroundColor: "green",
    zIndex: 99999,
    padding: 10,
    borderRadius: 60,
  },
  itemInfo: {
    flex: 2,
    flexDirection: "column",
    top: deviceHeight * 0.02,
    left: 10,
    //height: deviceHeight*.09
  },
  itemHeader: {
    fontSize: Theme.headerTextSize,
    fontWeight: "bold",
  },
  itemDetails: {
    width: deviceWidth * 0.6,
    fontSize: Theme.textSize,
    color: "gray",
  },
  itemPrice: {
    fontSize: 20,
    fontWeight: "bold",
    color: "#71c90c",
  },
  itemAction: {
    flex: 1,
    flexDirection: "column",
    top: 20,
    //alignContent: 'space-between'
    left: 15,
    // backgroundColor: 'red'
  },
});

export default styles;
