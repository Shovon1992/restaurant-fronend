import { AsyncStorage } from "react-native";

export const SELECTTABLE = "SELECTTABLE";

export const selectTable = (table) => {
  // console.log(table);
  return async (dispatch) => {
    try {
      var scanTablesOld = await AsyncStorage.getItem("scanTables");

      var scanTables = [
        {
          restaurantId: table.id,
          restaurantName: table.restaurantName,
          tableId: table.table,
          tableNumber: table.tableNumber,
        },
      ];

      if (scanTablesOld != null) {
        scanTablesOld = JSON.parse(scanTablesOld);
        AsyncStorage.removeItem("scanTables");

        for (let i = 0; i < scanTablesOld.length; i++) {
          if (
            scanTablesOld[i].restaurantId != table.id ||
            scanTablesOld[i].tableId != table.table
          ) {
            scanTables.push(scanTablesOld[i]);
          }
        }
      }

      // console.log(scanTables);
      // console.log("table scans : ", scanTablesOld);

      AsyncStorage.setItem("scanTables", JSON.stringify(scanTables));

      dispatch({
        type: SELECTTABLE,
        data: table,
      });
    } catch (error) {
      console.log(error);
      throw new Error(error.message);
    }
  };
};
