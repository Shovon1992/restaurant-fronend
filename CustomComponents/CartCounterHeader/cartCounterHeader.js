import React from 'react';
import { View, Text } from 'react-native';
import styles from './cartCounterHeaderStyle';

const cartCounterHeader = props => {
    return (
        <View style={styles.container}>
             <View style={styles.outerCircle}>
             <View style={styles.innerCircle} >
                    <Text style={styles.text}>{props.counter}</Text>
            </View>
            </View>
        </View>
    )
}

export default cartCounterHeader;