import React from "react";
import { View, Image, Text, TouchableWithoutFeedback } from "react-native";
import CustomButton from "../../CustomComponents/Button/Button";
import styles from "./HomeStyle";

const HomeScreen = (props) => {
  return (
    // <View style={styles.screen}>
    //     <View>
    //         <CustomButton onPress={()=>{props.navigation.navigate('ScanORCode')}} title='Scan QR ' />
    //     </View>
    //     <View style={{paddingTop : 20}}>
    //         <CustomButton onPress={()=>{props.navigation.navigate('Category')}} title='Go to Categories' />
    //     </View>
    // </View>

    <View style={styles.screen}>
      <View style={styles.headerLogo}>
        <Image
          source={require("../../assets/images/menume-logo.png")}
          style={styles.logo}
        ></Image>
      </View>
      <View style={styles.textContainer}>
        <Text style={styles.text}>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam, quis nostrud exercitation ullamco laboris nisi ut
          aliquip ex ea commodo consequat.
        </Text>
      </View>
      <View style={styles.buttonWrapper}>
        <TouchableWithoutFeedback
          onPress={() => {
            props.navigation.navigate("ScanORCode");
          }}
        >
          <View style={styles.buttonMainContainer}>
            <CustomButton
              onPress={() => {
                props.navigation.navigate("ScanORCode");
              }}
              title="Scan "
              style={styles.button}
            />
          </View>
        </TouchableWithoutFeedback>
      </View>
    </View>
  );
};

HomeScreen.navigationOptions = {
  headerTitle: "Welcome ",
};

export default HomeScreen;
