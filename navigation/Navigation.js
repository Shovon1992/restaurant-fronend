import React from "react";

import { createStackNavigator } from "react-navigation-stack";
import { createAppContainer, createSwitchNavigator } from "react-navigation";
import { createDrawerNavigator } from "react-navigation-drawer";

import WelcomeScreen from "../screens/WelcomeScreen/WelcomeScreen";

/** Food meals */
import MealCategoryScreen from "../screens/Meals/MealsCategoryScreen/MealCategoryScreen";
import FoodItems from "../screens/Meals/FoodItemList/FoodIems";

/** Auth */
import LoginSignupScreen from "../screens/Auth/LoginSignupScreen/LoginSignupScreen";
import SignupScreen from "../screens/Auth/SignupScreen/signup";
import ConfirmSignupScreen from "../screens/Auth/ConfirmSignupScreen/confirmSignupScreen";
import LoginScreen from "../screens/Auth/LoginScreen/loginScreen";
import LogoutDrawerButton from "../CustomComponents/LogoutButton/Logout";
import ForgotPasswordScreen from "../screens/Auth/ForgotPasswordScreen/ForgotPasswordScreen";
import ResetPasswordScreen from "../screens/Auth/ResetPassword/ResetPasswordScreen";

import AboutScreen from "../screens/AboutScreen/AboutScreen";
import ContactUsScreen from "../screens/Drawer/ContactUsScreen/ContactUsScreen";
import QRCodeScannerScreen from "../screens/QRCodeScannerScreen/QRCodeScannerScreen";

import HomeScreen from "../screens/HomeScreen/HomeScreen";
import NotificationScreen from "../screens/NotificationScreen/NotificationScreen";
import CartScreen from "../screens/CartScreen/CartScreen";
import OrdersScreen from "../screens/OrdersScreen/OrdersScreen";
import ThankyouScreen from "../screens/ThankyouScreen/ThankyouScreen";

import PersistantLoginScreen from "../screens/Auth/PersistantLogin";

import Theme from "../constants/Theme";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

import HeaderLeftRightButton from "../CustomComponents/HeaderLeftRightButton/HeaderLeftRightButton";

/* default style for the navigation headers */
const defaultNavStackOption = {
  headerStyle: {
    backgroundColor: Theme.background,
  },
  headerTitleAlign: "center",
  headerTintColor: Theme.headerTextColor, //back button color
};

/* Main navigation Screen */
const MealsNavigator = createStackNavigator(
  {
    home: HomeScreen,
    QRCode: {
      screen: QRCodeScannerScreen,
    },
    Welcome: WelcomeScreen,
    Category: {
      screen: MealCategoryScreen,
      navigationOptions: HeaderLeftRightButton,
    },
    Foods: {
      screen: FoodItems,
      navigationOptions: HeaderLeftRightButton,
    },
    Cart: {
      screen: CartScreen,
      // navigationOptions: HeaderLeftRightButton,
    },
    Notification: {
      screen: NotificationScreen,
      navigationOptions: HeaderLeftRightButton,
    },
    Thankyou: {
      screen: ThankyouScreen,
      //navigationOptions : HeaderLeftRightButton
    },
  },
  {
    navigationOptions: {
      drawerIcon: (drawerConfig) => (
        <Icon name="home" size={23} color={drawerConfig.tintColor} />
      ),
    },
    defaultNavigationOptions: defaultNavStackOption,
  }
);

//Create a stack navigator/Page to be used on the drawer tab AboutUs Page
const AboutPage = createStackNavigator(
  {
    About: {
      screen: AboutScreen,
      navigationOptions: HeaderLeftRightButton,
    },
  },
  {
    navigationOptions: {
      drawerIcon: (drawerConfig) => (
        <Icon name="help-network" size={23} color={drawerConfig.tintColor} />
      ),
    },
    defaultNavigationOptions: defaultNavStackOption,
  }
);

//Create a stack navigator/Page to be used on the drawer tab Contact Us Page
const ContactUsPage = createStackNavigator(
  {
    ContactUs: {
      screen: ContactUsScreen,
      navigationOptions: HeaderLeftRightButton,
    },
  },
  {
    navigationOptions: {
      drawerIcon: (drawerConfig) => (
        <Icon name="contacts" size={23} color={drawerConfig.tintColor} />
      ),
    },
    defaultNavigationOptions: defaultNavStackOption,
  }
);

//Create a stack navigator/Page to be used on the drawer tab Contact Us Page
const QRCodeScan = createStackNavigator(
  {
    QRCode: {
      screen: QRCodeScannerScreen,
      navigationOptions: HeaderLeftRightButton,
    },
  },
  {
    navigationOptions: {
      drawerIcon: (drawerConfig) => (
        <Icon name="qrcode" size={23} color={drawerConfig.tintColor} />
      ),
    },
    defaultNavigationOptions: defaultNavStackOption,
  }
);
//Create a stack navigator/Page to be used on the drawer tab Contact Us Page
const OrdersPage = createStackNavigator(
  {
    Orders: {
      screen: OrdersScreen,
      navigationOptions: HeaderLeftRightButton,
    },
  },
  {
    navigationOptions: {
      drawerIcon: (drawerConfig) => (
        <Icon name="cart" size={23} color={drawerConfig.tintColor} />
      ),
    },
    defaultNavigationOptions: defaultNavStackOption,
  }
);

//Create a stack navigator/Page to be used on the drawer tab Contact Us Page
const MealCatagoryPage = createStackNavigator(
  {
    Orders: {
      screen: MealCategoryScreen,
      navigationOptions: HeaderLeftRightButton,
    },
  },
  {
    navigationOptions: {
      drawerIcon: (drawerConfig) => (
        <Icon name="food-variant" size={23} color={drawerConfig.tintColor} />
      ),
    },
    defaultNavigationOptions: defaultNavStackOption,
  }
);
//Drawer navigator
const DrawerNavigator = createDrawerNavigator(
  {
    Meals: {
      screen: MealsNavigator,
      navigationOptions: {
        drawerLabel: "Menu",
      },
    },
    // Meallist: {
    //   screen: MealCatagoryPage,
    //   navigationOptions: {
    //     drawerLabel: "Meals",
    //   },
    // },

    About: {
      screen: AboutPage,
      navigationOptions: {
        drawerLabel: "About ",
      },
    },
    ContactUs: {
      screen: ContactUsPage,
      navigationOptions: {
        drawerLabel: "Contact Us ",
      },
    },
    ScanORCode: {
      screen: QRCodeScan,
      navigationOptions: {
        drawerLabel: "Scan QR Code",
      },
    },
    Orders: {
      screen: OrdersPage,
      navigationOptions: {
        drawerLabel: "Orders",
      },
    },
  },
  {
    contentOptions: {
      activeTintColor: Theme.accentColor,
      itemStyle: {
        flexDirection: "row",
      },
    },
    contentComponent: (props) => {
      return <LogoutDrawerButton property={props} />;
    },
  }
);

/* Auth Screens */

const AuthScreenNavigator = createStackNavigator(
  {
    LoginSignup: LoginSignupScreen,
    Login: LoginScreen,
    Signup: SignupScreen,
    ConfirmSignup: ConfirmSignupScreen,
    ForgotPassword: ForgotPasswordScreen,
    ResetPassword: ResetPasswordScreen,
  },
  {
    defaultNavigationOptions: defaultNavStackOption,
  }
);

/*Swtich navigator mainly for Auth and Switch navigator always stays on top on heicherchy */
const MainNavigator = createSwitchNavigator({
  Startup: PersistantLoginScreen,
  Auth: AuthScreenNavigator,
  Drawer: DrawerNavigator,
});

export default createAppContainer(MainNavigator);
