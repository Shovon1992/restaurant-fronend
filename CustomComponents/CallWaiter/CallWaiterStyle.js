import React from "react";
import { StyleSheet } from "react-native";
import Theme from "../../constants/Theme";

const styles = StyleSheet.create({
  waiterButtonView: {
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
  },
  waiterButtonIconView: {
    backgroundColor: Theme.greenColor,
    borderRadius: 20,
    height: 60,
    width: 60,
    justifyContent: "center",
    borderRadius: 100,
    marginRight: -10,
    zIndex: 9999999,
    alignItems: "center",
    borderWidth: 2,
    borderColor: "#fff",
    // elevation: 2,
  },
  waiterButtonTextView: {
    backgroundColor: Theme.greenColor,
    height: 40,
    width: 100,
    justifyContent: "center",
    alignItems: "center",
    paddingLeft: 3,
    borderTopRightRadius: 100,
    borderBottomRightRadius: 100,
    // elevation: 2,
  },

  modal: {
    width: Theme.deviceWidth,
    padding: 0,
    height: Theme.deviceWidth * 0.5,
    margin: 0,
  },
  modalContent: {
    height: 200,
    width: Theme.deviceWidth,
    marginTop: "auto",
    backgroundColor: "#fff",
    position: "relative",
    // paddingTop: 30,
    padding: 10,
    paddingTop: 20,
    justifyContent: "center",
  },
  modalCloseButton: {
    width: 35,
    height: 30,
    position: "absolute",
    right: 0,
    top: 5,
    // backgroundColor: "red",
  },
  customMessageView: { flexDirection: "row", padding: 0, marginTop: 20 },
  messageInputView: { width: Theme.deviceWidth * 0.7 },
  messageInput: {
    paddingHorizontal: 10,
    height: 50,
    borderRadius: 5,
    color: Theme.headerTextColor,
  },
  buttonView: {
    width: Theme.deviceWidth * 0.21,
    marginHorizontal: 10,
    // height: 50,
    // justifyContent: "center",
    // alignItems: "center",
    // backgroundColor: Theme.greenColor,
    // borderRadius: 10,
  },
  buttonText: {
    color: "#fff",
    textTransform: "uppercase",
    // fontWeight: "bold",
    fontSize: 17,
  },
});

export default styles;
