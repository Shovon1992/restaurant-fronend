let base_url =
  "https://cwwgzezy54.execute-api.us-east-1.amazonaws.com/dev/api/dev/";

export default {
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
  },
  advanceLogin: base_url + "user/login",
  userInfo: base_url + "user/info/data",
  restaurantWelcome: base_url + "resturant/welcome",
  foodCatagory: base_url + "resturant/food/catagory",
  foodItems: base_url + "resturant/food/item",
  cart: base_url + "user/cart",
  cartFetch: base_url + "user/cart",
  order: base_url + "user/order",
  orderDetails: base_url + "order/details",
  callWaiter: base_url + "call/waiter",
  getNotification: base_url + "notification/list/user",
  bookTable: base_url + "resturant/table/booking",
};
