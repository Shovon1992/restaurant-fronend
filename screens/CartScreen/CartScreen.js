import React, { useState, useEffect, useCallback } from "react";
import styles from "./CartStyle";
import {
  View,
  Text,
  ImageBackground,
  FlatList,
  TextInput,
  ScrollView,
} from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";

import { useSelector, useDispatch } from "react-redux";
import { Ionicons } from "@expo/vector-icons";
import Loader from "../../CustomComponents/Loader/loader";

import * as cartActions from "../../store/actions/cartAction";
import * as orderActions from "../../store/actions/orderAction";
import EmptyPlaceholder from "../../CustomComponents/Common/EmptyPlaceholder";
import Theme from "../../constants/Theme";
/** Main part to  */
const CartScreen = (props) => {
  const dispatch = useDispatch();
  const [isLoading, setIsLoading] = useState(false);
  const [isRefreshing, setIsRefreshing] = useState(false);

  const cartItems = useSelector((state) => {
    const transformedCartItems = [];
    for (const key in state.cart.items) {
      //console.log('state.cart.items screen',state.cart.items);
      transformedCartItems.push({
        id: key,
        item: state.cart.items[key].item,
        price: state.cart.items[key].price,
        //  priceId: state.cart.items[key].priceId,
        quantity: state.cart.items[key].quantity,
        image: state.cart.items[key].image,
        sum: state.cart.items[key].price,
        foodItemId: state.cart.items[key].foodItemId,
        priceId: state.cart.items[key].priceId,
        cartId: state.cart.items[key].cartId,
      });
    }

    return transformedCartItems.sort((a, b) => (a.id > b.id ? 1 : -1));
  });
  // console.log('cartItems',cartItems);

  const cartTotal = useSelector((state) => state.cart.totalAmount);

  const loadProducts = useCallback(async () => {
    setIsLoading(true);
    setIsRefreshing(true);
    try {
      await dispatch(cartActions.fetchCartItems());
    } catch (err) {
      console.log(err);
    }
    setIsRefreshing(false);
    setIsLoading(false);
  }, [dispatch, setIsLoading, setIsRefreshing]);

  /* Resist Rerender for loading products */
  useEffect(() => {
    loadProducts();
  }, [dispatch, loadProducts]);

  /* Place Order Button Handler */
  const orderHandler = async () => {
    setIsLoading(true);
    try {
      setIsLoading(false);
      await dispatch(orderActions.addOrder(cartItems, cartTotal));
      props.navigation.navigate("Thankyou");
    } catch (error) {
      setIsLoading(false);
      console.log(error);
    }
  };

  const cartLists = (itemData) => {
    // console.log('itemData',itemData);
    return (
      <View style={styles.gridItems}>
        <ImageBackground
          source={{
            uri: itemData.item.image
              ? itemData.item.image
              : "https://menume-images.s3.amazonaws.com/public/restaurant/welcomeScreen/WhatsApp%20Image%202020-07-31%20at%207.47.05%20PM.jpeg",
          }}
          style={styles.itemImage}
        ></ImageBackground>

        <View style={styles.itemInfo}>
          <View>
            <Text style={styles.itemHeader}>{itemData.item.item}</Text>
          </View>
          <View style={{ flexDirection: "row" }}>
            <TouchableOpacity
              style={styles.quantityView}
              onPress={() => {
                dispatch(
                  cartActions.removeQtyFromCart(itemData.item.id, itemData.item)
                );
              }}
            >
              <Ionicons
                name="md-remove"
                size={22}
                style={styles.quantityButton}
              ></Ionicons>
            </TouchableOpacity>
            <View style={{ marginHorizontal: 10 }}>
              <Text
                style={{
                  fontWeight: "bold",
                  fontSize: 22,
                  // color: Theme.greenColor,
                }}
              >
                {itemData.item.quantity}
              </Text>
            </View>
            <TouchableOpacity
              style={styles.quantityView}
              onPress={() => {
                dispatch(
                  cartActions.addQtyToCart(itemData.item.id, itemData.item)
                );
              }}
            >
              <Ionicons
                name="md-add"
                size={22}
                style={styles.quantityButton}
              ></Ionicons>
            </TouchableOpacity>
          </View>
        </View>

        <View style={styles.itemAction}>
          <View>
            <Text style={styles.itemPrice}>
              RS {itemData.item.sum.toFixed(2)}{" "}
            </Text>
          </View>
        </View>
      </View>
    );
  };

  if (isLoading) {
    return <Loader></Loader>;
  } else {
    if (cartItems.length !== 0) {
      // console.log('cartItems screen',cartItems);
      return (
        <View style={{ flex: 1 }}>
          <FlatList
            onRefresh={loadProducts}
            refreshing={isRefreshing}
            // keyExtractor={(item,index)=>item.id}
            data={cartItems}
            renderItem={cartLists}
            numColumns={1}
          />
          <ScrollView>
            <View style={styles.summarySection}>
              <View style={styles.sugesstionContainer}>
                <Text style={{ paddingBottom: 5 }}>
                  Suggestion for the Cook:{" "}
                </Text>
                <TextInput
                  style={styles.TextInputStyleClass}
                  underlineColorAndroid="transparent"
                  numberOfLines={3}
                  multiline={true}
                />
              </View>

              <View style={styles.itemSummary}>
                <View style={{ ...styles.commonItems }}>
                  <Text style={styles.commonText}>Item Total </Text>
                  <Text style={styles.commonText}>
                    RS {cartTotal.toFixed(2)}{" "}
                  </Text>
                </View>
                <View style={{ ...styles.commonItems }}>
                  <Text style={styles.commonText}>Coupon Discount </Text>
                  <Text style={{ ...styles.totalPrice, ...styles.commonText }}>
                    APPLIED{" "}
                  </Text>
                </View>
                <View style={{ ...styles.commonItems }}>
                  <Text style={styles.commonText}>Delivery Charge </Text>
                  <Text style={styles.commonText}>free </Text>
                </View>
              </View>

              <View style={styles.TotalConatainer}>
                <Text style={styles.itemHeader}>Total </Text>
                <Text style={{ ...styles.itemHeader, ...styles.totalPrice }}>
                  RS {cartTotal.toFixed(2)}{" "}
                </Text>
              </View>
            </View>
          </ScrollView>
          <View style={styles.proceedBtn}>
            <TouchableOpacity onPress={orderHandler}>
              <Text style={styles.proceedText}>Proceed to Checkout</Text>
            </TouchableOpacity>
          </View>
        </View>
      );
    } else {
      return (
        <EmptyPlaceholder
          page="cart"
          navigation={props.navigation}
        ></EmptyPlaceholder>
      );
    }
  }
};
CartScreen.navigationOptions = {
  headerTitle: "My Cart ",
};
export default CartScreen;
